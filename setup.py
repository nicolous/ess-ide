# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Various setup
python setup.py for more information"""

from distutils.core import setup

import glob
import os
import zipfile
import sys

# Locales to update with setup.py locales & setup.py mo
LOCALES = ("fr",)

def enum_files(directory, ext = ""):
    """Recursively enumerate files"""
    filelist = []
    for f in glob.glob("%s/*" % directory):
        if os.path.isdir(f):
            filelist.extend(enum_files(f, ext))
        elif os.path.basename(f).endswith(ext):
            filelist.append(f)
    return filelist
    
def make_zip(output_file, dir_to_zip):
    """Zip a dir"""
    message = "Creating %s: " % output_file
    
    dir_to_zip_len = len(dir_to_zip) + 1
    zip_file = zipfile.ZipFile(output_file, "w", compression=zipfile.ZIP_DEFLATED)
    files = enum_files(dir_to_zip)
    count = len(files)
    done = 0.0
    for f in files:
        zip_file.write(f, f[dir_to_zip_len:])
        print "\r%s%s%%" % (message, int(done / count * 100)),
        done += 1.0
    zip_file.close()
    
    print "\r%s100%%" % message    
    
    
def help():
    # Usage
    print "%s py2exe - Generates a Windows executable" % sys.argv[0]
    print "%s locales - Update the po translation files" % sys.argv[0]
    print "%s mo - Compile the po files into mo files" % sys.argv[0]
    
if len(sys.argv) == 1:
    help()

elif sys.argv[1] == "py2exe":
    # Generate an Windows executable (so no need to install Python + wxPython)
    
    import py2exe
    import main
    import common

    # py2exe.isSystemDLL override to force py2exe to include msvcp71.dll
    old_is_system_dll = py2exe.build_exe.isSystemDLL
    def is_file_system(pathname):
        res = 0
        if os.path.basename(pathname).lower() != "msvcp71.dll":
            res = old_is_system_dll(pathname)
        return res
    py2exe.build_exe.isSystemDLL = is_file_system

    print "Generating..."
    files = [(os.path.dirname(f), [f]) for f in enum_files("resources")]
    files.append((".", ["COPYING",]))
    files.append((".", ["changelog.txt",]))
    setup(
        version = common.IDE_VERSION,
        description = common.IDE_DESCRIPTION,
        name = common.IDE_NAME,

        # [(dirname, file list to copy to dist),]
        data_files = files,

        # targets to build
        windows = [{
            "script" : "ide.py",
            "icon_resources" : [(1, common.IDE_ICON)]
            }],
        )
        
    zipname = "win_ess-ide_v%s.zip" % common.IDE_VERSION
    if os.path.isfile(zipname):
        os.remove(zipname)
    make_zip(zipname, "dist")

    print "Done"
    
elif sys.argv[1] == "locales":
    # Update all locale files
    
    print "ess-ide.pot..."
    os.system("xgettext -k_ --from-code=utf-8 -o ess-ide.pot %s"
        % " ".join(enum_files(".", "py")))
    
    for loc in LOCALES:
        podir = "resources/locale/%s/LC_MESSAGES" % loc
        pofile = "%s/ess-ide.po" % podir

        print "ess-ide.pot => %s" % pofile
        if not os.path.isdir(podir):
            os.makedirs(podir)
            os.system("msginit -i ess-ide.pot -o %s" % pofile)
        elif not os.path.isfile(pofile):
            os.system("msginit -i ess-ide.pot -o %s" % pofile)    
        os.system("msgmerge -U %s ess-ide.pot" % pofile)

        print 
        print "Run %s mo to compile the po files" % sys.argv[0]
        
elif sys.argv[1] == "mo":
    # Compile all locale files
    
    for loc in LOCALES:
        pofile = "resources/locale/%s/LC_MESSAGES/ess-ide.po" % loc
        mofile = "resources/locale/%s/LC_MESSAGES/ess-ide.mo" % loc
        
        print "ess-ide.pot => %s" % mofile
        os.system("msgfmt %s -o %s" % (pofile, mofile))
    
else:
    help()    
