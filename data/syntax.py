# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""resources/syntax/*.xml reader
See ess.xml for more info"""

import xml.dom.minidom as minidom
import sys

class SyntaxReader(object):
    
    @staticmethod
    def _ToColor(string):
        """Convert a string to a color representation we support (color name or rgb tuple)"""
        rgb = string.split(" ")
        if len(rgb) == 3:
            valid = True
            for composant in rgb:
                valid &= composant.isdigit() and 0 <= int(composant) <= 255
            if valid:
                rgb = [int(x) for x in rgb if x.isdigit()]
            else:
                rgb = string
        else:
            rgb = string
        return rgb
        
    
    def __init__(self, filepath):
        """User defined styles reader
        filepath: XML file path
        """
        # Style defined
        self._styles = []
        
        # Command defined
        self._tokens = []        
        try:
            root = minidom.parse(filepath).documentElement
    
            for style_elt in root.childNodes:
                if style_elt.nodeType == minidom.Node.ELEMENT_NODE:
                    try:
                        style = Style()
                        style.foreground = SyntaxReader._ToColor(
                            style_elt.getElementsByTagName("foreground")[0].firstChild.data)
                        style.background = SyntaxReader._ToColor(
                            style_elt.getElementsByTagName("background")[0].firstChild.data)
                        style.bold = style_elt.getElementsByTagName("bold")[0].firstChild.data == "1"
                        style.italic = style_elt.getElementsByTagName("italic")[0].firstChild.data == "1"
                        style.underline = style_elt.getElementsByTagName("underline")[0].firstChild.data == "1"                    
                         
                        for token_elt in style_elt.getElementsByTagName("token"):
                            if token_elt.nodeType == minidom.Node.ELEMENT_NODE:
                                token = Token()
                                token.style = style
                                token.name = token_elt.getAttribute("name")
                                token.expand = token_elt.getAttribute("expand")
                                
                                for argsElt in token_elt.getElementsByTagName("args"):
                                    if token_elt.nodeType == minidom.Node.ELEMENT_NODE:
                                        if argsElt.firstChild is None:
                                            token.args.append("")                                    
                                        else:
                                            token.args.append(argsElt.firstChild.data)
                                
                                self._tokens.append(token)
                    except IndexError: # invalid style declaration
                        print >>sys.stderr, filepath, "warning: invalid style declaration" 
                        # TODO: XML validation (schema/DTD)
                            
                    self._styles.append(style)
        except IOError, e:
            print >>sys.stderr, "Unable to read",filepath, "(%s)" % e
        except BaseException,e:
            print >>sys.stderr, "Failed to parse",filepath,"(%s)" % e
            
                
    def FindToken(self, name):
        """Find a syntax.Token instance by name"""
        found = None # token found
        cmpname = name.lower().decode("utf_8")
        for token in self._tokens:
            if token.name == cmpname:
                found = token
                break
        return found
        
  
    def GetStyles(self):
        """Get all defined styles"""
        return self._styles
        
    def GetTokens(self):
        """Get all defined tokens"""
        return self._tokens

        
class Style(object):
    def __init__(self):
        self.foreground = "BLACK"
        self.background = "BLACK"        
        self.bold = False
        self.italic = False
        self.underline = False        
        
             
class Token(object):
    def __init__(self):
        self.style = None # syntax.Style instance
        self.name = "" # token name (e.g.: es_msg, equalto, ...)
        self.expand = False # does this token used to expand server_var/event_var?
        self.args = [] # if the token is a command name, all arguments accepted

        
    

                


