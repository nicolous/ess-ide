# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Profiles manager

All this code is probably temporary, until the project support is added"""

import os
import sys
import glob
import xml.dom.minidom as minidom

def get_profiles():
    """Returns a list of all available profiles"""
    basedir = os.path.dirname(sys.argv[0])
    return glob.glob(os.path.join(basedir, "resources/profiles/*.xml"))

class Profile(object):
    """Profile handler"""
    class Font(object):
        """<font> node"""
        def __init__(self, name, size):
            self.name = name
            self.size = size
            
    class Parsing(object):
        """<parsing> node"""
        def __init__(self, on_open_file, on_idle, when_relevant):
            self.on_open_file = on_open_file
            self.on_idle = on_idle
            self.when_relevant = when_relevant
            
    class Completion(object):
        """<completion> node"""
        def __init__(self, modal_only):
            self.modal_only = modal_only
            
    class Calltip(object):
        """<calltip> node"""
        def __init__(self, modal_only):
            self.modal_only = modal_only
            
    class Indent(object):
        """<indent> node"""
        def __init__(self, modal_only):
            self.modal_only = modal_only
            
    class Ftp(object):
        """<ftp> node"""
        class Server(object):
            """<server> node"""
            def __init__(self, name, ip, port, gamedir):
                self.name = name
                self.ip = ip
                self.port = port
                self.gamedir = gamedir
        
        def __init__(self, serverlist):
            self.serverlist = serverlist
                
    class Console(object):
        """<console> node"""
        class Server(object):
            """<server> node"""
            def __init__(self, name, ip, port, rcon_password):
                self.name = name
                self.ip = ip
                self.port = port
                self.rcon_password = rcon_password
        
        def __init__(self, ip, port, serverlist):
            self.ip = ip
            self.port = port
            self.serverlist = serverlist
         
    
    def __init__(self, name):
        """
        name: The profile name (e.g.: "default")
        """
        basedir = os.path.dirname(sys.argv[0])
        filepath = os.path.join(basedir, "resources/profiles/%s.xml" % name)
        try:
            root = minidom.parse(filepath).documentElement
            
            font = root.getElementsByTagName("font")[0]
            self.font = Profile.Font(
                            font.getElementsByTagName("name")[0].firstChild.data,
                            int(font.getElementsByTagName("size")[0].firstChild.data)
                            )
            
            parsing = root.getElementsByTagName("parsing")[0]
            self.parsing = Profile.Parsing(
                            int(parsing.getElementsByTagName("on_open_file")[0].firstChild.data),
                            int(parsing.getElementsByTagName("on_idle")[0].firstChild.data),
                            int(parsing.getElementsByTagName("when_relevant")[0].firstChild.data)
                            )
            completion = root.getElementsByTagName("completion")[0]
            self.completion = Profile.Completion(
                             int(completion.getElementsByTagName("modal_only")[0].firstChild.data)
                            )
            calltip = root.getElementsByTagName("calltip")[0]
            self.calltip = Profile.Calltip(
                             int(calltip.getElementsByTagName("modal_only")[0].firstChild.data)
                            )
            indent = root.getElementsByTagName("indent")[0]
            self.indent = Profile.Indent(
                             int(indent.getElementsByTagName("modal_only")[0].firstChild.data)
                            )
            ftp = root.getElementsByTagName("ftp")[0]
            serverlist = []
            for server in ftp.getElementsByTagName("server"):
                name = server.getElementsByTagName("name")[0].firstChild
                ip = server.getElementsByTagName("ip")[0].firstChild
                port = server.getElementsByTagName("port")[0].firstChild
                gamedir = server.getElementsByTagName("gamedir")[0].firstChild
                serverlist.append(Profile.Ftp.Server(
                                    name.data,
                                    ip.data,
                                    port.data,
                                    gamedir.data
                                  ))
            self.ftp = Profile.Ftp(serverlist)
        
            console = root.getElementsByTagName("console")[0]
            attr_ip =  console.getAttribute("ip")
            attr_port =  console.getAttribute("port")
            serverlist = []
            for server in console.getElementsByTagName("server"):
                name = server.getElementsByTagName("name")[0].firstChild
                ip = server.getElementsByTagName("ip")[0].firstChild
                port = server.getElementsByTagName("port")[0].firstChild
                rcon_password = server.getElementsByTagName("rcon_password")[0].firstChild
                serverlist.append(Profile.Console.Server(
                                    name.data,
                                    ip.data,
                                    port.data,
                                    rcon_password.data
                                  ))
            self.console = Profile.Console(attr_ip, attr_port, serverlist)
        except IndexError, e: # TODO: XML validation
            print >>sys.stderr, filepath," seems corrupted (%s)" % e            
        except IOError,e:
            print >>sys.stderr, "Unable to read", filepath, "(%s)" % e
        #except BaseException, e:
        #    print >>sys.stderr, "Failed to parse", filepath, "(%s)" % e            

