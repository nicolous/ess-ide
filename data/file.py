# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Files info"""

from __future__ import with_statement
import codecs

import common

class File(object):
    """Abstraction of a file"""
    
    def __init__(self, filepath = None):
        """Base constructor - Does not load/create the file
        Default file encoding is utf_8
        filepath: File path
        """
        self._filepath = filepath
        
        self._encoding = "utf_8" # file encoding
        self._bom = None # file BOM
        
    
    def Load(self):
        """Load the file from the disk
        Returns the file content encoded to utf-8
        Throws an IOError exceptions
        """
        with open(self._filepath,"r") as f:
            # Search for a bom
            head = f.readline()
            text = head + f.read()
            # utf-8
            if head.startswith(codecs.BOM_UTF8):
                self._bom = codecs.BOM_UTF8
                self._encoding = "utf_8"               
            # utf-16 little endian
            elif head.startswith(codecs.BOM_UTF16_LE):
                raise IOError("Unsupported encoding (utf-16 little endian)")                
            # utf-16 big endian          
            elif head.startswith(codecs.BOM_UTF16_BE):                
                raise IOError("unsupported encoding (utf-16 big endian)")                
            # utf-32 little endian
            elif head.startswith(codecs.BOM_UTF32_LE):                
                raise IOError("unsupported encoding (utf-32 little endian)")                
            # utf-32 big endian
            elif head.startswith(codecs.BOM_UTF32_BE):
                raise IOError("unsupported encoding (utf-32 big endian)")                

            # Try to decode with bom
            if self._bom is not None:                
                try:                    
                    print "bom: try", self._encoding
                    text = text[len(self._bom):].decode(self._encoding)
                except UnicodeDecodeError:                
                    raise IOError("error on decoding file bom")                
            else:
                # Try to decode the text as ascii                
                try:
                    print "try ascii"
                    text = text.decode("ascii")
                    self._encoding = "ascii"
                except UnicodeDecodeError:                                               
                    # Try to decode the text as utf-8
                    try:
                        print "try utf_8"
                        text = text.decode("utf_8")
                        self._encoding = "utf_8"
                    except UnicodeDecodeError:                                               
                        # Try to guess the encoding
                        for codec in common.IDE_SUPPORTED_CODECS:
                            try:
                                print "try",codec
                                text = text.decode(codec)
                                self._encoding = codec
                                break
                            except UnicodeDecodeError:
                                raise IOError("unsupported encoding")
            print "file encoding:", self._encoding   
            
        return text
    
    
    def Save(self, data):
        """Save the file
        data: The new file content
        Throws an IOError or an UnicodeEncodeError"""
        with open(self._filepath,"w") as f:
            if self._bom is not None:
                f.write(self._bom)        
            f.write(data)
    
    
    def SetPath(self, filepath):
        """Sets the file path"""
        self._filepath = filepath
    
    def GetPath(self):
        """Returns the file path"""
        return self._filepath
        
        
    def SetEncoding(self, bom, encoding):
        """Sets the file encoding
        bom: Encoding bom (can be None)
        encoding: Encoding name"""
        self._bom = bom
        self._encoding = encoding

    
    def GetEncoding(self):
        """Returns a (bom,encoding) tuple"""
        return (self._bom, self._encoding)
    
