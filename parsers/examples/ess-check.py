#    Copyright 2009-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESSParser.
#
#    ESSParser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESSParser is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESSParser.  If not, see <http://www.gnu.org/licenses/>.


# Test application
# For more information, type: python ess-check.py

from __future__ import with_statement
import sys
import codecs
import os
import timeit

sys.path.append("..")
import ess.esspreproc as esspreproc
import ess.esslexer as esslexer
import ess.essparser as essparser
import output.consoleoutput as consoleoutput
import output.xmloutput as xmloutput

OUT_DIR = "out"

def main():
    argc = len(sys.argv)

    if argc not in (2,3):
        print "%s script-to-check" % sys.argv[0]
        
    else:
        filepath = sys.argv[1] 
        debug = (argc > 2) and int(sys.argv[2])
        output = consoleoutput.ConsoleOutput()
        #output = xmloutput.XmlOutput()

        # "out" is the debug output dir
        if not os.path.isdir(OUT_DIR):
            os.mkdir(OUT_DIR)
        
        try:
            with codecs.open(filepath,"r","utf-8-sig") as f:
                # Create the parse tools
                preproc = esspreproc.EssPreProcessor(output)
                lexer = esslexer.EssLexer(output,debug)
                parser = essparser.EssParser(output,debug,lexer)
                   
                # First, preparse the script, so comments & BOM are removed
                code = preproc.parse(filepath,f.read(),debug)
                if debug:
                    with open("%s/%s.preproc" % (OUT_DIR,os.path.basename(filepath)),"w") as preprocfile:
                        preprocfile.write(code)
                        
                # Then, parse the code
                codeObj = parser.parse(filepath,code,debug)

                if debug and (codeObj is not None):
                    with open("%s/%s.memory" % (OUT_DIR,os.path.basename(filepath)),"w") as objfile:
                        objfile.write(codeObj.dump())
                        
                # About the results: 
                print
                print "%s: %s errors, %s warnings" % (filepath,output.errorCount(),output.warningCount())
                if isinstance(output,xmloutput.XmlOutput):
                    print output
            
        except IOError,e:
            sys.stderr.write("%s\n" % e)
    
    
if __name__ == "__main__":
    print "\nExecution time: %.2f seconds" % timeit.Timer("main()","from __main__ import main").timeit(1)    
