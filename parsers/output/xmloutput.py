#    Copyright 2009-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESSParser.
#
#    ESSParser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESSParser is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESSParser.  If not, see <http://www.gnu.org/licenses/>.

from baseoutput import *

import xml.dom.minidom as minidom

class XmlOutput(BaseOutput):
    """XML output

    <essparser">
        <test/es_test.txt>
            <error>
                <line>37</line>
                <message>Error description</message>
            </error>
            <warning>
                <line>1</line>
                <message>Warning description</message>
            <notice>
                <line>1</line>
                <message>Notice description</message>
            </warning>
        </test/es_test.txt>
    </essparser>
    """

    def __init__(self):
        BaseOutput.__init__(self)

        self.__dom = minidom.getDOMImplementation()
        self.__document = None
        self.__root = None
        self.clear() # initialize document & root
        
        self.clear()


    def __getScriptNode(self,scriptpath):
        scriptNode = None
        
        existing = self.__root.getElementsByTagName(scriptpath)
        if len(existing) == 0:
            scriptNode = self.__document.createElement(scriptpath)
            self.__root.appendChild(scriptNode)
        else:
            scriptNode = existing[0]

        return scriptNode


    def __createNode(self,problem,scriptpath,lineno,message):
        linenoText = self.__document.createTextNode(str(lineno))
        linenoNode = self.__document.createElement("line")
        linenoNode.appendChild(linenoText)

        messageText = self.__document.createTextNode(message)
        messageNode = self.__document.createElement("message")
        messageNode.appendChild(messageText)

        typeNode = self.__document.createElement(problem)
        typeNode.appendChild(linenoNode)
        typeNode.appendChild(messageNode)

        scriptNode = self.__getScriptNode(scriptpath)
        scriptNode.appendChild(typeNode)


    def __repr__(self):
        return self.__document.toprettyxml()

    def getXml(self):
        """Returns a brut XML string containing all outputs"""
        return self.__document.toxml("utf-8")

    def getDocument(self):
        """Returns the XML document"""
        return self.__document
        

    def warningCount(self):
        return len(self.__root.getElementsByTagName("warning"))

    def errorCount(self):
        return len(self.__root.getElementsByTagName("error"))

    def noticeCount(self):
        return len(self.__root.getElementsByTagName("notice"))

    
    def clear(self):
        self.__document = self.__dom.createDocument(None,"essparser",None)
        self.__root = self.__document.documentElement


    def warning(self,scriptpath,lineno,message):
        self.__createNode("warning",scriptpath,lineno,message)

    def error(self,scriptpath,lineno,message):
        self.__createNode("error",scriptpath,lineno,message)

    def notice(self,scriptpath,lineno,message):
        self.__createNode("notice",scriptpath,lineno,message)
        
