
# Defines _ if the application which uses the parser is not internationalized
import __builtin__

if not __builtin__.__dict__.has_key("_"):
    __builtin__.__dict__["_"] = lambda text: text

