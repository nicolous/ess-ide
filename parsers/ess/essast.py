#    Copyright 2009-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESSParser.
#
#    ESSParser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESSParser is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESSParser.  If not, see <http://www.gnu.org/licenses/>.

"""Abstract Syntax Tree structures (parser output)"""

import os

class Script(object):
    """Script info"""
    
    def __init__(self,path,blocklist):
        """
        path : Script path
        blocklist : essast.Block list
        """        
#        self.__path = path.replace("\\","/")
        self.__path = path
        self.__blocklist = blocklist
        
        # Can iterate over the block list
        self.__itercount = 0
        
    def next(self):
        if self.__itercount >= len(self.__blocklist):
            self.__itercount = 0
            raise StopIteration

        block = self.__blocklist[self.__itercount]
        self.__itercount += 1                             
        return block   
        
    def getName(self):
        """Get the script basename"""
        name = os.path.basename(self.__path)
        if name.startswith("es_"):
            name = name[3:]
        if name.endswith(".txt"):
            name = name[:-4]
        return name

    def getPath(self):
        """Get the script path"""
        return self.__path

    def getBlocks(self):
        """Get all the script's blocks.
        Blocks are instance of essast.Block instances."""
        return self.__blocklist

    def dump(self):
        """Get a string representation of the script"""
        output = self.__path
        output += "\n\n"
        for block in self.__blocklist:
            output += "\t"
            output += block.dump()
            output += "\n\n"
        return output
    
    def __repr__(self):
        return "%s: [%s]" % (self.__path,", ".join([block.getName() for block in self.__blocklist]))
    
    def __str__(self):
        return repr(self)


class Block(object):
    """Block info"""
    
    def __init__(self,blockname,lineno,definition):
        """
        blockname : Block name
        lineno : Where the block is defined
        definition : The code (CommandLine/Block instance list)
        """
        self.__blockname = blockname
        self.__lineno = lineno
        self.__definition = definition
        
        # Can iterate over the code lines
        self.__itercount = 0
        
    def next(self):
        if self.__itercount >= len(self.__definition):
            self.__itercount = 0
            raise StopIteration

        code = self.__definition[self.__itercount]
        self.__itercount += 1                             
        return code

    def getName(self):
        """Get the block name"""
        return self.__blockname

    def getLineno(self):
        """Get the line number where the block is defined"""
        return self.__lineno

    def getDefinition(self):
        """Get the definition of the block.
        Definition can be either a essast.CommandLine list or a essast.Block list for subblocks"""
        return self.__definition
        
    def dump(self):
        """Get a string representation of the block"""
        #return "block %s" % self.__blockname
        output = self.__blockname if self.__blockname is not None else ""
        output += "\n"
        for code in self.__definition:
            output += "\t\t"
            output += code.dump()
            output += "\n"
        return output
    
    def __repr__(self):
        return "block %s" % self.__blockname
    
    def __str__(self):
        return repr(self)
        
        
class Event(Block):
    """"Event info"""
    
    def dump(self):
        """Get a string representation of the block"""
        return "event %s" % Block.dump(self)
    
    def __repr__(self):
        return "event %s" % self.getName()
        

class CommandLine(object):
    """Command line info"""

    def __init__(self,lineno,line):
        """
        lineno : Where this command line is defined
        line : Argument list
        """
        self.__lineno = lineno
        self.__line = line

    def getLineno(self):
        """Get the line number where the command line is defined"""
        return self.__lineno

    def getLine(self):
        """Get the argument list corresponding to this command line.
        All arguments are instances of str."""
        return self.__line

    def setLine(self,newline):
        """Used by the parser to defines what's the code of the command line."""
        self.__line = newline

    def dump(self):
        """Get a string representation of the CommandLine"""
        return "Line %s:%s" % (self.__lineno,self.__line)
    
    def __repr__(self):
        return self.dump()
    
    def __str__(self):
        return repr(self)
        
