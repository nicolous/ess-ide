# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Code which is potentially useful in any file"""

IDE_NAME = "ESS IDE"

IDE_VERSION = "1.6a"

IDE_DESCRIPTION = _("IDE for EventScripts Shell")

IDE_COPYRIGHT = "Distributed under the GNU GPL license version 3."

IDE_LICENCE = """%s is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

%s is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with %s.  If not, see <http://www.gnu.org/licenses/>.""" % ((IDE_NAME,)*3)

IDE_ICON = "resources/images/mascot.ico"

IDE_MASCOT = "resources/images/mascot.png"

IDE_WEBSITE = "http://bitbucket.org/nicolous/ess-ide"

IDE_DEVELOPERS = (
    "Nicolas \"Nicolous\" Maingot <dani31m@hotmail.com>",
    )
    
IDE_DOC_WRITERS = tuple(
    )
    
IDE_ARTISTS = tuple(
    )    
    
IDE_TRANSLATORS = tuple(
    )
    
# http://docs.python.org/library/codecs.html    
IDE_SUPPORTED_CODECS = (
    # "ascii", # natively supported
    # "utf_8", # natively supported (all is internally encoded as utf-8)
    "latin_1",
    "cp1252",
    )
    
# http://docs.wxwidgets.org/2.6/wx_wxfont.html    
#IDE_CODECS_FONTCODING = {
#    "ascii" : wx.FONTENCODING_DEFAULT,
#    "utf_8" : wx.FONTENCODING_UTF8,
#    "latin_1" : wx.FONTENCODING_ISO8859_1,        
#    "cp1252" : wx.FONTENCODING_CP1252,
#}

# Directory that contains the following files:
#  version.txt - contains the last version name available
#  ess-ide_v<version name here>.zip AND win_ess-ide_v<version name here>.zip - files to update (the win_*.zip one is the Windows binary release)
IDE_UPDATE_URL = "http://www.cssmatch.com/ess-ide"

TEXT_LOGO = "resources/images/text-logo.png"

ESS_LOGO = "resources/images/ess-logo.png"
ESS_SYNTAX = "resources/syntax/ess.xml"

KV_LOGO = "resources/images/kv-logo.png"

PY_LOGO = "resources/images/py-logo.png"
