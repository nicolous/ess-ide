# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Update notifier"""

import threading
import urllib
import wx.lib.newevent

import common

# Custom events: 
#    Update available
UpdateAvailableEvent, EVT_UPDATE_AVAILABLE = wx.lib.newevent.NewEvent()

class UpdateNotifier(threading.Thread):
    """Notify the user that an update is available"""
    
    def __init__(self, app, frame):
        """
        app : wx Application instance
        frame : wx events will be sent to this frame 
        """
        threading.Thread.__init__(self)
        self._app = app
        self._frame = frame
        
    def run(self):
        try:
            # Check for an update
            version = urllib.urlopen("%s/version.txt" % common.IDE_UPDATE_URL).readline().strip()
            
            # Does the application still runs?
            if self._app.IsMainLoopRunning():
                if version != common.IDE_VERSION:
                    print "Update available! (Current version: %s; New version:%s)" % (common.IDE_VERSION, version)
                    
                    # Notify the user
                    wx.PostEvent(self._frame, UpdateAvailableEvent(version=version))
        except IOError,e:
            print "Update notifier failed: %s" % e
