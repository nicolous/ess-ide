# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

import os
import sys
import glob
import time
import shutil

import wx
import wx.stc

import common
import data.syntax as syntax
import widgets.editor as editor
import widgets.managers.manager as manager
import widgets.dialogs.essrefactoring as essrefactoring


import ess.esspreproc as esspreproc
import ess.esslexer as esslexer
import ess.essparser as essparser
import ess.essast as essast
import output.xmloutput as xmloutput

import kv.kvpreproc as kvpreproc
import kv.kvlexer as kvlexer
import kv.kvparser as kvparser
import output.silentoutput as silentoutput

import ess2py.pytranslator as builder
import ess2py.pycallbacks as builder_callbacks

"""ESS source code manager classes"""


def read_events():
    """Read all .res files in resources/games/*
    Event names and info are memorized and used in some code checks
    Returns a ({event name : set(info list)}, set(info list)) tuple"""
    events = {} # event dictionnary to return
    eventinfos = set() # event infos set to return
    
    output = silentoutput.SilentOutput() # parser output
    preproc = kvpreproc.KvPreProcessor(output) # preprocessor
    lexer = kvlexer.KvLexer(output, False) # lexer
    parser = kvparser.KvParser(output, False, lexer) # parser
    
    for folder in glob.glob(os.path.join(os.path.dirname(sys.argv[0]), "resources/games/*")):
        if os.path.isdir(folder):
            for res in glob.glob("%s/*.res" % folder):
                try:
                    with open(res) as f:
                        keygroup = parser.parse(res, preproc.parse(res, f.read())) # parser object ouput 
                        if keygroup is not None and output.errorCount() == 0:
                            for key in keygroup.getKeys():
                                if isinstance(key, kvparser.Key):
                                    event = key.getName() # event name
                                    infos = set( # info names set
                                                [keyvalue.getName() for keyvalue in key.getValues() 
                                                 if isinstance(keyvalue, kvparser.KeyValue)]
                                                ) 
                                    if "userid" in infos:
                                        infos.update(("es_username",
                                                      "es_steamid",
                                                      "es_userweapon",
                                                      "es_userteam",
                                                      "es_userarmor",
                                                      "es_userhealth",
                                                      "es_userdeaths",
                                                      "es_userkills",
                                                      "es_userdead",
                                                      "es_userindex"))
                                    if "attacker" in infos:
                                        infos.update(("es_attackername",
                                                      "es_attackersteamid",
                                                      "es_attackerweapon",
                                                      "es_attackerteam",
                                                      "es_attackerarmor",
                                                      "es_attackerhealth",
                                                      "es_attackerdeaths",
                                                      "es_attackerkills",
                                                      "es_attackerdead",
                                                      "es_attackerindex"))
                                    infos.add("es_event")
                                    
                                    stored = events.get(event, None) # do we already know this key?
                                    if stored is not None:
                                        stored.update(infos)
                                    else:
                                        events[event] = infos
                                    eventinfos.update(infos)
                        else:
                            print >>sys.stderr, "%s seems to be corrupted" % res
                except IOError, e:
                    print >>sys.stderr, "Unable to read %s (%s)" % (res,e)
    
    return events, eventinfos  


class EssCodeManager(manager.BaseCodeManager):
    """ESS source code manager"""
    
    # Highlight styles
    HL_KEYWORDS = 10
    HL_BRACKET = 11
    HL_COMMENT = 12
    HL_QUOTED = 13
    HL_ESCAPECHARS = 14
    HL_EVENTS = 15
    HL_EVENTINFOS = 16
    HL_USER_STYLE_BASE = 20
    
    # Indicators
    IND_ERRORS = 0 #  wx.stc.STC_INDIC0_MASK
    IND_WARNINGS = 1 #  wx.stc.STC_INDIC1_MASK
    #IND_AVAILABLE = 2 #  wx.stc.STC_INDIC2_MASK    

    # Highlighted lex tokens
    KEYWORDS = {
        "BLOCK" : HL_KEYWORDS,
        "EVENT" : HL_KEYWORDS,
        }
    DELIMITERS = {
        "LBRACKET" : HL_BRACKET,
        "RBRACKET" : HL_BRACKET,
        }
    COMMENTS = {
        "COMMENT" : HL_COMMENT,
        }
    
    # Highlighted lex values
    ESCAPECHARS = {
        #"{" : HL_ESCAPECHARS,
        #"}" : HL_ESCAPECHARS,       
       "(" : HL_ESCAPECHARS,
       ")" : HL_ESCAPECHARS,
       ";" : HL_ESCAPECHARS,
       "'" : HL_ESCAPECHARS,
       ":" : HL_ESCAPECHARS,
       }
    
    # Marker ids
    #MK_ERROR = 0
    #MK_WARNING = 1
    
    # User styles
    _syntax_reader = syntax.SyntaxReader(
                        os.path.join(os.path.dirname(sys.argv[0]), common.ESS_SYNTAX))
    
    # Events data (see readEvents above)
    _events, _eventinfos = read_events()
    
    @staticmethod 
    def GetName():
        """See widgets.managers.manager.BaseCodeManager"""
        return _("ESS")    
    
    @staticmethod
    def GetFormatInfo():
        """See widgets.managers.manager.BaseCodeManager"""
        return ("es_",".txt")
    
    @staticmethod
    def GetLogo():
        """See widgets.managers.manager.BaseCodeManager"""
        return common.ESS_LOGO
     
    
    def __init__(self, my_editor):
        """See widgets.managers.manager.BaseCodeManager"""
        manager.BaseCodeManager.__init__(self, my_editor)

        # Parser output
        self._parser_output = xmloutput.XmlOutput()

        # ESS preprocessor
        self._preproc = esspreproc.EssPreProcessor(self._parser_output)

        # ESS lexer
        self._lexer = esslexer.EssLexer(self._parser_output, False)

        # ESS parser
        self._parser = essparser.EssParser(self._parser_output, False ,self._lexer)
        
        # ESS to PY code generator output
        self._builder_output = xmloutput.XmlOutput()
        
        # ESS to PY code generator
        self._builder = builder.EssTranslator(self._builder_output)
        # Install all known "ess command to py code" callbacks
        for callback in builder_callbacks.command_callbacks:
            self._builder.addCallback(callback, builder_callbacks.command_callbacks[callback])

        # Parser result
        self._objcode = None
        
        self._lexer.input("") # DOCUMENT ME: don't recall why
                
        # Last user activity (currently, last time the user pressed a key)
        self._last_activity_time = time.time()
        
        # Does the code has been parsed since the last user activity?
        self._checked = False

        # Register some images used in the auto-completion windows
        basedir = os.path.dirname(sys.argv[0]) # application directory                
        # ... for the event names
        my_editor.RegisterImage(
            1,
            wx.Image(
                    os.path.join(basedir, "resources/images/event-16.png"),
                    wx.BITMAP_TYPE_PNG
                    ).ConvertToBitmap()
                )
        # ... for the event info and the variable names
        my_editor.RegisterImage(
            2,
            wx.Image(
                    os.path.join(basedir, "resources/images/event-info-16.png"),
                    wx.BITMAP_TYPE_PNG
                    ).ConvertToBitmap()
                )
        # ... for the tokens defined by the user (see ess.xml)
        my_editor.RegisterImage(
            3,
            wx.Image(
                os.path.join(basedir, "resources/images/command-16.png"),
                wx.BITMAP_TYPE_PNG
                    ).ConvertToBitmap()
                )
        # ... for the block names
        my_editor.RegisterImage(
            4,
            wx.Image(
                os.path.join(basedir, "resources/images/block-16.png"),
                wx.BITMAP_TYPE_PNG
                    ).ConvertToBitmap()
                )
        

    def Start(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        # Init the styles
        #   Keywords style
        my_editor.StyleSetForeground(EssCodeManager.HL_KEYWORDS, "MEDIUM BLUE")
        my_editor.StyleSetBold(EssCodeManager.HL_KEYWORDS, True)

        #   Brackets style
        my_editor.StyleSetForeground(EssCodeManager.HL_BRACKET, "RED")
        my_editor.StyleSetBold(EssCodeManager.HL_BRACKET, True)

        #   Comment style
        my_editor.StyleSetForeground(EssCodeManager.HL_COMMENT, wx.Color(0, 128, 0))
        my_editor.StyleSetItalic(EssCodeManager.HL_COMMENT, True)

        #   Quote style
        my_editor.StyleSetForeground(EssCodeManager.HL_QUOTED, "RED")

        #   Escape chars style
        my_editor.StyleSetForeground(EssCodeManager.HL_ESCAPECHARS, "RED")
        my_editor.StyleSetBold(EssCodeManager.HL_ESCAPECHARS, True)

        #   Event names style
        my_editor.StyleSetForeground(EssCodeManager.HL_EVENTS, "BROWN")  

        #   Event infos style
        my_editor.StyleSetForeground(EssCodeManager.HL_EVENTINFOS, "MAROON")
        
        #   Matching braces highlight
        my_editor.StyleSetForeground(wx.stc.STC_STYLE_BRACELIGHT, "RED")
        my_editor.StyleSetBold(wx.stc.STC_STYLE_BRACELIGHT, True)
        my_editor.StyleSetBackground(wx.stc.STC_STYLE_BRACELIGHT, "YELLOW")             
        my_editor.StyleSetForeground(wx.stc.STC_STYLE_BRACEBAD, "RED")
        my_editor.StyleSetBold(wx.stc.STC_STYLE_BRACEBAD, True)
        my_editor.StyleSetBackground(wx.stc.STC_STYLE_BRACEBAD,"ORANGE")        

        # Error/Warning indicators
        my_editor.IndicatorSetStyle(EssCodeManager.IND_ERRORS, wx.stc.STC_INDIC_SQUIGGLE)
        my_editor.IndicatorSetForeground(EssCodeManager.IND_ERRORS, "RED")
        my_editor.IndicatorSetStyle(EssCodeManager.IND_WARNINGS, wx.stc.STC_INDIC_SQUIGGLE)
        my_editor.IndicatorSetForeground(EssCodeManager.IND_WARNINGS, wx.Color(255, 165, 0)) # orange
        
        # Init the user defined styles (ess.xml)
        self._user_styles = {}
        style_id = EssCodeManager.HL_USER_STYLE_BASE # next style id
        for style in EssCodeManager._syntax_reader.GetStyles():
            my_editor.StyleSetForeground(style_id,style.foreground)
            my_editor.StyleSetBackground(style_id,style.background)            
            my_editor.StyleSetBold(style_id,style.bold)        
            my_editor.StyleSetItalic(style_id,style.italic)            
            my_editor.StyleSetUnderline(style_id,style.underline)                        
            self._user_styles[style] = style_id
            style_id += 1

        # Handle text change
        my_editor.Bind(wx.stc.EVT_STC_CHARADDED, self.OnCharAdded)
        my_editor.Bind(wx.EVT_CHAR, self.OnChar)
        my_editor.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        my_editor.Bind(wx.stc.EVT_STC_UPDATEUI, self.OnUpdateUi)
        my_editor.Bind(wx.EVT_IDLE, self.OnIdle)
        my_editor.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
        my_editor.Bind(wx.EVT_SCROLLWIN, self.OnScroll)
                        
    
    def Stop(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        my_editor.StyleClearAll()
        
        my_editor.Unbind(wx.stc.EVT_STC_CHARADDED)
        my_editor.Unbind(wx.EVT_CHAR)
        my_editor.Unbind(wx.EVT_KEY_DOWN)
        my_editor.Unbind(wx.stc.EVT_STC_UPDATEUI)
        my_editor.Unbind(wx.EVT_IDLE)
        my_editor.Unbind(wx.EVT_KILL_FOCUS)
        my_editor.Unbind(wx.EVT_SCROLLWIN)
        

    def Comment(self):
        """See widgets.managers.manager.BaseCodeManager"""    
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        my_editor.BeginUndoAction()
        
        startpos = my_editor.GetSelectionStart()
        endpos = my_editor.GetSelectionEnd()
        from_line = my_editor.LineFromPosition(startpos)
        to_line = my_editor.LineFromPosition(endpos)

        # For each line, add "//" after any leading space
        for linenum in xrange(from_line, to_line + 1):
            line = my_editor.GetLineUTF8(linenum).rstrip() # rstrip to remove \r & \n
            line_len = len(line)
            pos = my_editor.PositionFromLine(linenum)
            i = 0
            if line_len > 0:
                while i < line_len and line[i].isspace():
                    i += 1
            my_editor.InsertTextUTF8(pos + i, "//")
            
        # Restore the selection
        new_startpos = my_editor.PositionFromLine(from_line)
        text = my_editor.GetLineUTF8(to_line).rstrip() # rtrip to strip \r\n
        new_endpos = my_editor.PositionFromLine(to_line) + len(text)
        my_editor.SetSelection(new_startpos, new_endpos)
        
        my_editor.EndUndoAction()
            
    def Uncomment(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        my_editor.BeginUndoAction()
        
        startpos = my_editor.GetSelectionStart()
        endpos = my_editor.GetSelectionEnd()
        from_line = my_editor.LineFromPosition(startpos)
        to_line = my_editor.LineFromPosition(endpos)

        # For each line starting with "//", remove "//"
        for linenum in xrange(from_line, to_line + 1):
            linepos = my_editor.PositionFromLine(linenum)
            text = my_editor.GetLineUTF8(linenum)
            comment_pos = text.find("//")
            if comment_pos == 0 or (comment_pos != -1 and text[:comment_pos].isspace()):
                my_editor.SetSelection(linepos + comment_pos, linepos + comment_pos + 2)
                my_editor.Clear()
          
        # Restore the selection          
        new_startpos = my_editor.PositionFromLine(from_line)
        text = my_editor.GetLineUTF8(to_line).rstrip() # rtrip to strip \r\n
        new_endpos = my_editor.PositionFromLine(to_line) + len(text)
        my_editor.SetSelection(new_startpos, new_endpos)
          
        my_editor.EndUndoAction()
          

    def Parse(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        start = 0 # start position
        end = my_editor.GetTextLength() # end position

        self._checked = True        
        
        fileobj = my_editor.GetFile()
        filepath = fileobj.GetPath()
        if filepath is None:
            filepath = "not saved"
            
        # Remove any previous error/warning
        my_editor.RemoveErrors()  
        self.RemoveIndicators(start, end)
        
        # Lex & parse it
        self._parser_output.clear()
        code = my_editor.GetText()[start:end] # code to parse (unicode to be compliant with gettext)
        
        source = self._preproc.parse(filepath, code)
        if fileobj.GetEncoding()[0] is not None:
            self._parser_output.warning(
                filepath,
                1,
                _("You should not encode your script in UTF *with* BOM, that could confuse EventScripts"))
        
        objcode = self._parser.parse(filepath, source, False)
        if objcode is not None:
            self._objcode = objcode
        
        # Show output
        self._parseXmlOutput(self._parser_output)

    
    def Build(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        fileobj = my_editor.GetFile()        
        filepath = fileobj.GetPath()
        if filepath is None:
            my_editor.Save()
            filepath = fileobj.GetPath()
        
        if filepath is not None: 
            filedir = os.path.dirname(filepath)               
            if self._objcode is None:
                self.Parse()
                
            if self._objcode is not None:
                self._builder_output.clear()
                self._builder.translate(self._objcode)
                    
                # Show output
                self._parseXmlOutput(self._builder_output)
                
                # Open generated file + its emlib.py (dependance)
                emlibfile = os.path.join(filedir, "emlib.py")                
                shutil.copy(os.path.join("builders", "ess2py", "resources", "emlib.py"), emlibfile)
                pyfile = os.path.join(filedir, "%s.py" % os.path.basename(filepath)[3:~3])
                if os.path.isfile(emlibfile) and \
                       os.path.isfile(pyfile):
                    frame = my_editor.GetFrame()
                    editor_tabs = frame.GetEditorTabs()
                    editor_tabs.NewPage(os.path.basename(emlibfile), None, emlibfile)
                    editor_tabs.NewPage(os.path.basename(pyfile), None, pyfile)


    def _parseXmlOutput(self, xml_output):
        """Parse the XML doc of an xmloutput.XmlOutput object"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        # Get/Analyze the (xml) parser output (see parsers/output/xmloutput.py)
        errordoc = xml_output.getDocument()
        for error in errordoc.getElementsByTagName("error"):
            line = error.getElementsByTagName("line")[0].childNodes[0].data
            message = error.getElementsByTagName("message")[0].childNodes[0].data
            my_editor.ReportError("error", line, message)
            self._HlError(int(line) - 1)
            
        for warning in errordoc.getElementsByTagName("warning"):
            line = warning.getElementsByTagName("line")[0].childNodes[0].data
            message = warning.getElementsByTagName("message")[0].childNodes[0].data 
            my_editor.ReportError("warning", line, message)
            self._HlWarning(int(line) - 1)        
   
    def _HlError(self, linenum):
        """Highlight an error line"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        start = my_editor.PositionFromLine(linenum)
        text = my_editor.GetLineUTF8(linenum)
        my_editor.StartStyling(start, wx.stc.STC_INDIC0_MASK)
        my_editor.SetStyling(len(text), wx.stc.STC_INDIC0_MASK)

    def _HlWarning(self,linenum):
        """Warn the user about a line"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        start = my_editor.PositionFromLine(linenum)
        text = my_editor.GetLineUTF8(linenum)
        my_editor.StartStyling(start, wx.stc.STC_INDIC1_MASK)
        my_editor.SetStyling(len(text), wx.stc.STC_INDIC1_MASK)    
        
    
    def RemoveIndicators(self, start, end):
        """Remove all error indicators from start to end"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance

        my_editor.StartStyling(start, wx.stc.STC_INDIC0_MASK | wx.stc.STC_INDIC1_MASK)
        my_editor.SetStyling(end, 0) # TODO: review 0 
        

    def Highlight(self, start, end):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        code = my_editor.GetTextUTF8()[start:end] # code to highlight 
        #code = my_editor.GetText()[start:end].rstrip().encode("utf_8")
        #code = my_editor.GetTextRangeUTF8(start, end)
#        print "code: '%s'" % code,len(code)

#        print "__highlight '%s'" % code
#        for match in re.finditer("\S+", code):
#            word = match.group()
#        toHl = code
#        print "  '%s'" % code
        self._lexer.input(code) # lexer.token() will return a string with the same encoding

        # While the lexer outputs tokens, highlight
        token = self._lexer.token()
        while token is not None:
#            print "    ",token
            style = None # style to apply to this token
            
            # Does the token correspond to an user defined token?
            user_token = EssCodeManager._syntax_reader.FindToken(token.value)
            if user_token is not None:
                style = self._user_styles[user_token.style]
            else:
                # Does the token correspond to a keyword?
                style = EssCodeManager.KEYWORDS.get(token.type,None)
                if style == None:
                    # Does the token correspond to a delimiter?
                    style = EssCodeManager.DELIMITERS.get(token.type,None)
                    if style == None:
                        # Does the token correspond to a comment statement
                        style = EssCodeManager.COMMENTS.get(token.type,None)
                        if style == None:
                            # Does the token correspond to an escape char?
                            style = EssCodeManager.ESCAPECHARS.get(token.value,None)                        
                            if style == None:
                                # Does the token correspond to an event name?
                                if token.value in EssCodeManager._events:
                                    style = EssCodeManager.HL_EVENTS
                                else:
                                    # Does the token correspond to an event info?
                                    if token.value in EssCodeManager._eventinfos:
                                        style = EssCodeManager.HL_EVENTINFOS
                                    else:
                                        # Does the token correspond to a quoted text?
                                        if token.value[0] == "\"" and \
                                           token.value[len(token.value) - 1] == "\"":
                                                style = EssCodeManager.HL_QUOTED
                                        else:
                                            # Nothing special found, default style will be used
                                            style = wx.stc.STC_STYLE_DEFAULT
                        
            # Style the code
            my_editor.StartStyling(start + token.lexpos, 0x1f)
            #  0x1f: only modify the text style bits http://www.yellowbrain.com/stc/textio.html#cells
            my_editor.SetStyling(len(token.value), style)

            token = self._lexer.token() # next token please


    def FixIndent(self, start, end):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        indent_settings = my_editor.GetIndent() # indent level setting
        
        my_editor.BeginUndoAction()
        
        linenum = start
        indent_level = 0 # indent level of the line we're reading
        while linenum < end:
            line = my_editor.GetLineUTF8(linenum).strip()
                        
            if line == "}":
                if indent_level > 0:
                    indent_level -= indent_settings
                    
            if my_editor.GetLineIndentation(linenum) != indent_level:
                my_editor.SetLineIndentation(linenum, indent_level)
            
            if line == "{":
                indent_level += indent_settings
            linenum += 1
            
        my_editor.EndUndoAction()
        
        
    def ExtractBlock(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        frame = my_editor.GetFrame() # Main frame
        dial = essrefactoring.ExtractBlockDialog(
                    my_editor, frame, wx.ID_ANY, _("Refactoring"))
        dial.ShowModal()
        dial.Destroy()


    def UpdateFolding(self, start, end):
        """See widgets.managers.manager.BaseCodeManager"""
        pass
#        lineStart = self.__my_editor.LineFromPosition(start)
#        lineEnd = self.__my_editor.LineFromPosition(end)
#        print "fold from",lineStart,"to",lineEnd
#        
#        levelPrevious = wx.stc.STC_FOLDLEVELBASE        
#        if lineStart > 0:
#            levelPrevious = self.__my_editor.GetFoldLevel(lineStart - 1) & (wx.stc.STC_FOLDLEVELNUMBERMASK) # >> 16
#        levelCurrent = levelPrevious
#        
#        linenum = lineStart
#        while linenum <= lineEnd:
#            line = self.__my_editor.GetLineUTF8(linenum).strip()
#                    
#            if line == "{":
#                levelCurrent += 1
#            elif line == "}":
#                levelCurrent -= 1            
#                
#            #levelUse = levelPrevious
#            lev = levelPrevious | levelCurrent & (~ wx.stc.STC_FOLDLEVELNUMBERMASK) # << 16
#            if len(line) == 0:
#                lev |= wx.stc.STC_FOLDLEVELWHITEFLAG
#            if levelPrevious < levelCurrent:
#                lev |= wx.stc.STC_FOLDLEVELHEADERFLAG
#            elif levelPrevious > levelCurrent:
#                lev |= ~wx.stc.STC_FOLDLEVELHEADERFLAG
#            if lev != self.__my_editor.GetFoldLevel(linenum):
##                print lev,lev & wx.stc.STC_FOLDLEVELNUMBERMASK
#                self.__my_editor.SetFoldLevel(linenum,lev)    
#                
#            linenum += 1
#            levelPrevious = levelCurrent
                      

    def _GetBlockNameFromLine(self, linenum):
        """Get the name of the block defined at a particular line number"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance      
        blockname = None # block name to return

        # Here self.__objcode is inappropriate because the context may have changed
        curline = linenum
        lbrackets = 0 # { count
        rbrackets = 0 # } count
        while curline >= 0:
            # Search for a block/event declaration previous the current line
            self._lexer.input(my_editor.GetLineUTF8(curline))
            token = self._lexer.token()
            if token is not None:
                if token.type in ("BLOCK","EVENT"):
                    if lbrackets > rbrackets: # if not, we're probably out of block
                        token = self._lexer.token()
                        if token is not None:
                            blockname = token.value # REVIEW ME: doesn't support block names with space
                    break
                else:
                    next_token = self._lexer.token()
                    if next_token is None or next_token.type == "EOL":
                        if token.type == "LBRACKET":
                            lbrackets += 1
                        elif token.type == "RBRACKET":
                            rbrackets += 1
            
            curline -= 1

        return blockname
    
    
    def _GetDoblocks(self, block):
        """Recursively returns all essast.CommandLine that corresponds to an es_[x]doblock in this block
        block: essast.Block instance
        """
        doblocks = [] # es_doblock command lines to return
        
        for instr in block.getDefinition():
            if isinstance(instr,essast.Block):
                doblocks.extend(self._GetDoblocks(instr))
            else:
                args = instr.getLine()
                if len(args) >= 2 and args[0] in ("es_doblock","es_xdoblock"):
                    doblocks.append(args)
        
        return doblocks
        
    
    def _GetCallHierarchies(self, blockname):
        """Returns all possible call hierarchies for a specific block"""
        def _CallHierarchies(blockname, viewed):
            """Does the recursive job
            viewed: all block name already visited"""
            hierarchies = [] # call hierarchies to return
            scriptname = self._objcode.getName() # the script name
            
            viewed.append(blockname)

            for tempblock in self._objcode.getBlocks():
                if tempblock.getName() not in viewed:
                    for doblock in self._GetDoblocks(tempblock):
                        # Is there an es_doblock that calls our block? 
                        iSep = doblock[1].rfind("/")
                        namespace = doblock[1][:iSep] # script name 
                        called = doblock[1][iSep + 1:] # block to call
                        if namespace == scriptname and called == blockname:
                            # Found a new block that calls our block
                            # Get its call hierarchies too
                            _hierarchies = _CallHierarchies(tempblock, viewed)
                            if _hierarchies:
                                for hierarchy in _hierarchies:
                                    hierarchies.append(hierarchy + [tempblock])
                            else:
                                hierarchies.append([tempblock])
                            break
            
            return hierarchies
        
        hierarchies = [] # call hierarchies to return
        if self._objcode is not None:
            hierarchies = _CallHierarchies(blockname,[])
        return hierarchies
    
    
    def GetBlockEventInfos(self, blockname):
        """Returns a set of the event infos supported by this block"""
        infos = None # infos set to return
        
        # Maybe the block is an instance of essast.Event
        infos = EssCodeManager._events.get(blockname, None)
        if infos is None:
            # Otherwise, check for an event that calls this block            
            infos = set()
            for hierarchy in self._GetCallHierarchies(blockname):
                for block in hierarchy:
                    if isinstance(block, essast.Event):
                        infos.update(EssCodeManager._events.get(block.getName(), set()))
        return infos
    
    
    def Tokenize(self, text):
        """Tokenizes a text - Returns a lexer token list
        Not optimized but usefull to avoid to lex the same code in multiple functions
        """
        tokens = [] # token list to return
        
        # Lex the current line from the begin of the line to the caret position
        self._lexer.input(text)
        token = self._lexer.token()
        while token is not None:
            tokens.append(token)
            token = self._lexer.token() 
            
        return tokens 
    
    
    def CompleteDoblock(self,tokens,space):
        """Tries to complete "es_doblock [script/]"
        tokens: Lex token list of the current line
        space: True if there is a space between the last token and the caret
        space defines if we'll complete a word the user is writing, or a entiere new word
        """
        success = False # success state to return
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        char_to_drop = 0 # char count to eat if we auto-complete
        suggestions = [] # suggestions to show
        
        if self._objcode is not None:
            scriptname = self._objcode.getName() # name of the script we're editing
            
            # Case "es_doblock "
            if space and tokens:
                if tokens[-1].value in ("es_doblock", "es_xdoblock"):
                    suggestions = ["%s/%s" % (scriptname, block.getName()) \
                                    for block in self._objcode.getBlocks() \
                                        if isinstance(block, essast.Block)]
                    
            # Case "es_doblock sometext"
            elif len(tokens) >= 2:
                if tokens[-2].value in ("es_doblock", "es_xdoblock"):
                    arg = tokens[-1].value # first command line arg
                    sep_pos = arg.rfind("/")
                    
                    # Case "es_doblock sometext" 
                    if sep_pos == -1:
                        if scriptname.startswith(arg):
                            pos = my_editor.GetCurrentPos()
                            my_editor.SetSelection(pos,pos - len(arg))
                            my_editor.ReplaceSelection("%s/" % scriptname)
                            suggestions = [block.getName() \
                                            for block in self._objcode.getBlocks() \
                                                if isinstance(block, essast.Block)]
                            
                    # Case "es_doblock namespace/sometext"
                    else:
                        blockname = arg[sep_pos + 1:] # "sometext" part
                        char_to_drop = len(blockname)                            
                        suggestions = [block.getName() \
                                        for block in self._objcode.getBlocks() \
                                            if isinstance(block, essast.Block) and \
                                               block.getName().startswith(blockname)]
                    
            if suggestions:
                # text: each suggestion followed by an image ?id
                text = "?4 ".join(sorted(suggestions))
                text += "?4"
                my_editor.AutoCompShow(char_to_drop, text)
            
                success = True
                    
        return success
        
        
    def CompleteEventVar(self, tokens, space):
        """Tries to complete "event_var("
        tokens: Lex token list of the current line
        space: True if there is a space between the last token and the caret        
        """
        success = False # success state to return
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        char_to_drop = 0 # char count to eat if we auto-complete        
        suggestions = set() # suggestions to show
        
        event_var = False # True if the user is writing "event_var(" 
        infoname = "" # info name to complete
        
        # Case "event_var("
        if len(tokens) >= 2 and tokens[-2].value == "event_var" and tokens[-1].value == "(":
            event_var = True
        # Case event_var(sometext
        elif len(tokens) >= 3 and tokens[-3].value == "event_var" and tokens[-2].value == "(":
            event_var = True
            infoname = tokens[-1].value # "sometext" part
            char_to_drop = len(infoname)

        if event_var:
            curblockname = self._GetBlockNameFromLine(my_editor.GetCurrentLine())
            if curblockname is not None:
                suggestions = set([info for info in self.GetBlockEventInfos(curblockname) \
                                   if info.startswith(infoname)])

            #if not suggestions:
            #    for value in EssCodeManager._events.values():
            #        suggestions.update(set([info for info in value \
            #                                if info.startswith(infoname)]))

            if suggestions:
                # text: each suggestion followed by an image ?id
                text = "?2 ".join(sorted(suggestions))
                text += "?2"
                my_editor.AutoCompShow(char_to_drop, text)
                
                success = True
                
        return success
    
    
    def CompleteServerVar(self, tokens, space):
        """Tries to complete "server_var("
        tokens: Lex token list of the current line
        space: True if there is a space between the last token and the caret        
        """
        success = False # success state to return
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        char_to_drop = 0 # char count to eat if we auto-complete        
        suggestions = [] # suggestions to show to the user

        server_var = False # True if the user is writing "server_var("
        varname = "" # var name to complete
        
        # Case "server_var"
        if len(tokens) >= 2 and tokens[-2].value == "server_var" and tokens[-1].value == "(":
            server_var = True
        # Case "server_var(sometext"
        elif len(tokens) >= 3 and tokens[-3].value == "server_var" and tokens[-2].value == "(":
            server_var = True
            varname = tokens[-1].value
            char_to_drop = len(varname)                

        if server_var:
            if varname:
                suggestions = [token.name for token in EssCodeManager._syntax_reader.GetTokens() \
                               if token.name.startswith(varname)]  
            else:              
                suggestions = [token.name for token in EssCodeManager._syntax_reader.GetTokens() \
                               if token.name.startswith("eventscripts_")] # trick!

            if suggestions:
                # text: each suggestion followed by an image ?id
                text = "?2 ".join(sorted(suggestions))
                text += "?2"
                my_editor.AutoCompShow(char_to_drop, text)
                
                success = True
                
        return success    
             
        
    def CompleteEventName(self, tokens, space):
        """Tries to complete "event "
        tokens: Lex token list of the current line
        space: True if there is a space between the last token and the caret
        """
        success = False # success state to return
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        char_to_drop = 0 # char count to eat if we auto-complete        
        suggestions = [] # suggestions to show to the user
        word = "" # event name the user is writing
        
        if tokens and tokens[0].type == "EVENT":
            # Case "event "
            if len(tokens) == 1 and space:
                suggestions = EssCodeManager._events.keys()
            # Case "event sometext"
            elif not space:
                    word = tokens[-1].value # "sometext" part
                    charToDrop = len(word) 
                    suggestions = [event for event in EssCodeManager._events.keys() \
                                    if event.startswith(word)]
                
            if suggestions:
                # text: each suggestion followed by an image ?id
                text = "?1 ".join(sorted(suggestions))
                text += "?1"
                my_editor.AutoCompShow(char_to_drop,text)
            # TODO: Let the user select what games will run their script
            # So we can just show the event supported by this game
            
            success = True
        
        return success       
        
    
    def CompleteCommandName(self, tokens, space):
        """Tries to complete the command name the user is writing
        tokens: Lex token list of the current line
        space: True if there is a space between the last token and the caret
        """
        success = False # success state to return
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        char_to_drop = 0 # char count to eat if we auto-complete        
        suggestions = [] # suggestions to show to the user
        word = "" # command the user is writing
        
        if tokens:
            if not space:
                # Get the last word the user wrote
                word = tokens[-1].value
                char_to_drop = len(word)
                suggestions = [token.name for token in EssCodeManager._syntax_reader.GetTokens() \
                               if token.name.startswith(word)]
        else:
            suggestions = [token.name for token in EssCodeManager._syntax_reader.GetTokens()]            
            
        if suggestions:
            # text: each suggestion followed by an image ?id
            text = "?3 ".join(sorted(suggestions))
            text += "?3"
            my_editor.AutoCompShow(char_to_drop, text)
            success = True
        
        return success
                
                        
    def CompleteAny(self):
        """Tries to auto-complete what the user is writing"""
        success = False # success state to return
        
        my_editor = self.GetMyEditor() # StyledTextCtrl instance

        # Compute the caret position in this line
        linenum = my_editor.GetCurrentLine()
        curline = my_editor.GetLineUTF8(linenum)
        linepos = my_editor.GetCurrentPos() - my_editor.PositionFromLine(linenum)

        text = curline[:linepos] # text of this line, until the caret position  
        tokens = self.Tokenize(text)
        space = text[-1].isspace() if text else False
        
        success = self.CompleteDoblock(tokens,space) or \
                  self.CompleteEventVar(tokens,space) or \
                  self.CompleteServerVar(tokens,space) or \
                  self.CompleteEventName(tokens,space) or \
                  self.CompleteCommandName(tokens,space)
        
        return success


    def Calltip(self):
        """Shows a calltip"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        pos = my_editor.GetCurrentPos()
        linenum = my_editor.LineFromPosition(pos)
        linepos = my_editor.PositionFromLine(linenum)
        line = my_editor.GetLineUTF8(linenum).strip()

        # Does the current line begins by an user defined token?
        self._lexer.input(line)
        cmd_token = self._lexer.token()
        if cmd_token is not None:
            user_token = EssCodeManager._syntax_reader.FindToken(cmd_token.value)
            if user_token is not None:
                # Get the arguments next to the token
                argc = 0 # arg count we already read
                
                # server_var(text) and event_var(text) have 4 arguments
                # expand_tokens will serve to count how many tokens of "server_var", "(", "text" and
                # ")" we encountered
                # if expand_tokens is 0, we can add 1 to argc, otherwise we're reading server_var(blah)/event_var(blah)...
                expand_tokens = 0
                token = self._lexer.token()
                while token is not None:
                    if token.value in ("server_var", "event_var"):
                        expand_tokens += 2
                    else:
                        if expand_tokens == 0:
                            argc += 1
                        else:
                            expand_tokens -= 1
                    token = self._lexer.token()
    
                # Keep only the calltips that could match what the user is typing (based on the argument count)
                matching = filter(lambda arglist:
                                      len(arglist.split()) >= argc or arglist.endswith("..."),
                                  user_token.args)

                if matching:
                    # Compute the part of the calltip to highlight
                    hlEnd = len(user_token.name)
                    for arg in matching[0].split()[:argc]:
                        hlEnd += 1 + len(arg)
                        
                    calltipStart = linepos + my_editor.GetLineIndentation(linenum) 
                    calltipText = "\n".join(("%s %s" % (user_token.name, syntax) for syntax in matching))
                    my_editor.CallTipShow(calltipStart, calltipText)                            
                    my_editor.CallTipSetHighlight(0, hlEnd)
                else:
                    my_editor.CallTipCancel()


    def OnCharAdded(self, event):
        """User added a char in the code
        Update the indentation if needed, and try to complete what the user is writing"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        profile = my_editor.GetProfile() # current user profile
        key = event.GetKey() # char the user added

        pos = my_editor.GetCurrentPos()
        linenum = my_editor.LineFromPosition(pos)
        currline = my_editor.GetLineUTF8(linenum)
        #currline = my_editor.GetLine(linenum).encode("utf_8")
        
        if not profile.completion.modal_only:
            # " adds or eats a "
            if chr(key) == "\"":
                if unichr(my_editor.GetCharAt(pos)) == u"\"":
                    my_editor.SetSelection(pos, pos + 1)
                    my_editor.Clear()
                else:
                    my_editor.InsertTextUTF8(pos, "\"")
    
            # ) eats a )
            elif chr(key) == ")":
                if unichr(my_editor.GetCharAt(pos)) == u")":
                    my_editor.SetSelection(pos, pos + 1)
                    my_editor.Clear()
    
            # { on a new line after a block declaration adds a }
            elif chr(key) == "{":
                if currline.strip() == "{":
                    tokens = self.Tokenize(my_editor.GetLineUTF8(linenum - 1))
                    if len(tokens) >= 2:
                        # Case "block " or "event " or "... do"
                        if tokens[0].type in ("BLOCK", "EVENT") or \
                           tokens[-2].value == "do":
                            indentLevel = my_editor.GetLineIndentation(linenum)
                                
                            my_editor.BeginUndoAction()
                            newIndentLevel = indentLevel + my_editor.GetIndent()
                            my_editor.NewLine()
                            my_editor.NewLine()
                            my_editor.InsertTextUTF8(pos + 2, "}")
                            my_editor.SetLineIndentation(linenum + 1, newIndentLevel)
                            my_editor.GotoPos(pos + 1 + newIndentLevel)
                            my_editor.SetLineIndentation(linenum + 2, indentLevel)
                            my_editor.EndUndoAction()
            
                            self.Highlight(pos - 1,my_editor.PositionFromLine(linenum + 2) + indentLevel + 1)

        # } on a new line will set the indentation to match the indentation level of the corresponding {
        if chr(key) == "}":
            if currline.strip() == "}":
                
                wx.Yield()
                # FIXME: trick
                # Even if the char is actually added, self.BraceMatch will not find the matching brace
                # So let stc process the pending events then we can find the matching brace
                
                matching = my_editor.BraceMatch(pos - 1) # matching { position
                if matching != wx.stc.STC_INVALID_POSITION:
                    matching_line = my_editor.LineFromPosition(matching)
                    indent_level = my_editor.GetLineIndentation(matching_line)

                    if not profile.indent.modal_only:
                        my_editor.SetLineIndentation(linenum,indentLevel)
                    
                    # If } is at the begin of a new line, then the user defines a new block
                    if indent_level == 0 and profile.parsing.when_relevant:
                        # Let SetLineIndentation works
                        wx.Yield()
                        # Then parse the code
                        self.Parse() 
                
        else: # the other cases need more info
            linepos = pos - my_editor.PositionFromLine(linenum)
            text = currline[:linepos] # text of this line, until the caret
            tokens = self.Tokenize(text)
            space = text[-1].isspace() if text else False

            # Try to complete a es_doblock on /
            if chr(key) == "/":
                self.CompleteDoblock(tokens, space)

            # ( adds a ), and try to complete an event info
            elif chr(key) == "(":
                my_editor.InsertTextUTF8(pos, ")")
                self.CompleteEventVar(tokens, space) #or self.completeServerVar(tokens, space)               

            # Try to complete an event name on space
            elif chr(key) == " ":
                self.CompleteEventName(tokens, space)
        #event.Skip()
        
        
    def OnChar(self,event):
        """User wrote a char
        Remove error indicators on the current line"""
        # Note that cannot be done in onCharAdded, or there is a conflict between
        #  onStyleNeeded and the indicators removing
        
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        pos = my_editor.GetCurrentPos()
        linenum = my_editor.LineFromPosition(pos)
        text = my_editor.GetLineUTF8(linenum)
        #text = my_editor.GetLine(linenum).encode("utf_8")

        start = my_editor.PositionFromLine(linenum)
        end = start + len(text)
        self.RemoveIndicators(start,end)
        
        event.Skip()   
                    
        
    def OnKeyDown(self, event): 
        """User pressed a key
        Update the indentation if needed, show auto-completion, show calltips
        Update the last activity time from the user"""        
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        profile = my_editor.GetProfile() # current user profile
        keycode = event.GetKeyCode() # the key code the user presses
        
        pos = my_editor.GetCurrentPos()
        skip = True # if this event has to be skipped or not
        
        self._last_activity_time = time.time()  
        self._checked = False        

        # ENTER may updates the indentation, and cancel any calltip
        # ENTER after a block/event declaration auto-complete the brackets
        if keycode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER) and (not my_editor.AutoCompActive()):
            # get the current line number and the text until the caret position
            linenum = my_editor.LineFromPosition(pos)
            linepos = my_editor.PositionFromLine(linenum)
            line = my_editor.GetLineUTF8(linenum).replace("\n", "").replace("\r", "")
            currline = line[:pos - linepos]

            # The indentation level stays at least the same than the previous line 
            indent_level = my_editor.GetLineIndentation(linenum)
            
            completed = False # True if we auto-complete something

            # { on a new line followed by ENTER increases the indentation
            # (excepted if we have something else on the line)
            if (currline.strip() == "{"):
                # If there is no matching }, auto-complete with a new }
                if not profile.completion.modal_only and \
                   my_editor.BraceMatch(pos - 1) == wx.stc.STC_INVALID_POSITION:
                    my_editor.BeginUndoAction()
                    new_indent_level = indent_level + my_editor.GetIndent()
                    my_editor.NewLine()
                    my_editor.NewLine()
                    my_editor.InsertTextUTF8(pos + 2,"}")
                    my_editor.SetLineIndentation(linenum + 1, new_indent_level)
                    my_editor.GotoPos(pos + 1 + new_indent_level)
                    my_editor.SetLineIndentation(linenum + 2, indent_level)
                    my_editor.EndUndoAction()

                    # highlight the text we added
                    self.Highlight(pos - 1, my_editor.PositionFromLine(linenum + 2) + indent_level + 1)
                    
                    # new block => reparse the code
                    if profile.parsing.when_relevant:
                        self.Parse()
                        
                    completed = True
                    skip = False
                    
                # Otherwise, just update the indent level
                elif not profile.indent.modal_only:
                    new_indent_level = indent_level + my_editor.GetIndent()
                    my_editor.BeginUndoAction()
                    if my_editor.GetSelectionStart() != my_editor.GetSelectionEnd():
                        my_editor.Clear()
                        my_editor.NewLine()
                        newline = my_editor.LineFromPosition(my_editor.GetCurrentPos())
                        my_editor.SetLineIndentation(newline, new_indent_level)
                        my_editor.GotoPos(my_editor.PositionFromLine(newline) + new_indent_level)
                    else:
                        my_editor.NewLine()
                        my_editor.SetLineIndentation(linenum + 1, new_indent_level)
                        my_editor.GotoPos(my_editor.PositionFromLine(linenum + 1) + new_indent_level)
                    my_editor.EndUndoAction()
                    
                    completed = True
                    skip = False
            
            # Try to find other things to complete 
            else:
                tokens = self.Tokenize(currline)
                
                if not profile.completion.modal_only:
                    # If the current line matches "event sometext" or "block sometext",
                    # or ends with "do", auto-complete with { }
                    if len(tokens) >= 2 and \
                       self._GetBlockNameFromLine(linenum) is None and \
                       tokens[0].type in ("BLOCK", "EVENT"):
                        my_editor.BeginUndoAction()
                        indent_level = my_editor.GetLineIndentation(linenum)
                        new_indent_level = indent_level + my_editor.GetIndent()

                        my_editor.NewLine()
                        my_editor.InsertTextUTF8(pos + 1, "{")
                        my_editor.GotoPos(pos + 2)
                        my_editor.NewLine()
                        my_editor.NewLine()
                        my_editor.InsertTextUTF8(pos + 4, "}")
                        my_editor.SetLineIndentation(linenum + 1, indent_level)
                        my_editor.SetLineIndentation(linenum + 2, new_indent_level)
                        my_editor.SetLineIndentation(linenum + 3, indent_level)
                        
                        new_line_pos = my_editor.PositionFromLine(linenum + 2)
                        my_editor.GotoPos(new_line_pos + new_indent_level)
                        my_editor.EndUndoAction()
                        
                        # Highlight the code we added
                        self.Highlight(pos, my_editor.PositionFromLine(linenum + 3) + indent_level + 1)
                        
                        # New block => reparse the code
                        if profile.parsing.when_relevant:
                            self.Parse()
                        
                        completed = True
                        skip = False
                        
                    elif len(tokens) >= 2 and tokens[-1].value == "do":
                        # REVIEW ME: if...do is case sensitive, else...do not
                        my_editor.BeginUndoAction()
                        indent_level = my_editor.GetLineIndentation(linenum)
                        new_indent_level = indent_level + my_editor.GetIndent()
                        
                        my_editor.NewLine()
                        my_editor.InsertTextUTF8(pos + 1, "{")
                        my_editor.GotoPos(pos + 2)
                        my_editor.NewLine()
                        my_editor.NewLine()
                        my_editor.InsertTextUTF8(pos + 4, "}")
                        my_editor.SetLineIndentation(linenum + 1, indent_level)
                        my_editor.SetLineIndentation(linenum + 2, new_indent_level)
                        my_editor.SetLineIndentation(linenum + 3, indent_level)
                        
                        newLinePos = my_editor.PositionFromLine(linenum + 2)
                        my_editor.GotoPos(newLinePos + new_indent_level)
                        my_editor.EndUndoAction()
                        
                        # Highlight the code we added
                        self.Highlight(pos,my_editor.PositionFromLine(linenum + 3) + indent_level + 1)
                        
                        #if profile.parsing.when_relevant:
                        #    self.parse()
                        
                        completed = True
                        skip = False
            
            # If we didn't find anything to auto-complete, just maintain the current indent level
            if not completed and not profile.indent.modal_only:
                my_editor.BeginUndoAction()
                if my_editor.GetSelectionStart() != my_editor.GetSelectionEnd():
                    my_editor.Clear()
                    my_editor.NewLine()
                    newline = my_editor.LineFromPosition(my_editor.GetCurrentPos())
                    my_editor.SetLineIndentation(newline, indent_level)
                    my_editor.GotoPos(my_editor.PositionFromLine(newline) + indent_level)
                else:
                    my_editor.NewLine()
                    my_editor.SetLineIndentation(linenum + 1, indent_level)
                    my_editor.GotoPos(my_editor.PositionFromLine(linenum + 1) + indent_level)
                my_editor.EndUndoAction()
                
                skip = False

            # Cancel any calltip
            if my_editor.CallTipCancel():
                my_editor.CallTipCancel()

        # CTRL+SPACE shows the auto-completion feature
        elif event.ControlDown() and keycode == wx.WXK_SPACE:
            if not self.CompleteAny():
                self.Calltip()

        # SPACE or TAB shows a calltip
        elif keycode == wx.WXK_SPACE or keycode == wx.WXK_TAB:
            if not profile.calltip.modal_only or my_editor.CallTipActive():
                self.Calltip()

        # HOME and END close the auto-completion window
        elif keycode in (wx.WXK_HOME,wx.WXK_END):
            if my_editor.AutoCompActive():
                my_editor.AutoCompCancel()
            

        # BACK may auto-remove some additional chars to help
        elif keycode == wx.WXK_BACK:
            linenum = my_editor.LineFromPosition(pos)
            
            # Remove ""
            if my_editor.GetTextRangeUTF8(pos - 1,pos + 1) == "\"\"":
                my_editor.SetSelection(pos,pos + 1)
                my_editor.Clear()

            # Remove a ()
            elif my_editor.GetTextRangeUTF8(pos - 1,pos + 1) == "()":
                my_editor.SetSelection(pos,pos + 1)
                my_editor.Clear()

            ## Remove {\n\n} 
            #prevline = my_editor.GetLineUTF8(linenum - 1).strip()
            #currline = my_editor.GetLineUTF8(linenum)
            #nextline = my_editor.GetLineUTF8(linenum + 1)
            #if prevline == "{" and \
            #   currline.isspace() and \
            #   nextline.strip() == "}":
            #    start = my_editor.PositionFromLine(linenum - 1) + 1 + my_editor.GetLineIndentation(linenum - 1)
            #    end = my_editor.PositionFromLine(linenum + 1) + len(nextline)
            #    my_editor.SetSelection(start, end)
            #    my_editor.Clear()

            #else: # other case need the line text
            #    currline = my_editor.GetLineUTF8(linenum)
            #    if currline.isspace():
            #        start = my_editor.PositionFromLine(linenum)
            #        my_editor.SetSelection(start, start + len(currline))
            #        my_editor.Clear()
                
        if skip:
            event.Skip()


    def OnUpdateUi(self,event):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        pos = my_editor.GetCurrentPos()

        brace_pos = wx.stc.STC_INVALID_POSITION # matching bracket position

        # Is there a bracket before the caret?
        char = my_editor.GetCharAt(pos - 1)
        if char > 0 and chr(char) in "{}":
            if my_editor.GetStyleAt(pos - 1) == EssCodeManager.HL_BRACKET:
                brace_pos = pos - 1
        else:
            # ...or maybe after?
            char = my_editor.GetCharAt(pos)
            if char > 0 and chr(char) in "{}":
                if my_editor.GetStyleAt(pos) == EssCodeManager.HL_BRACKET:
                    brace_pos = pos
        
        # Ok, there is a bracket near the caret and we know its position
        # Highlight any matching bracket 
        if brace_pos != wx.stc.STC_INVALID_POSITION:
            # Search for matching bracket
            match = my_editor.BraceMatch(brace_pos)
            
            # Highlight
            if match != wx.stc.STC_INVALID_POSITION:
                my_editor.BraceHighlight(brace_pos, match)
            #else:
            #    my_editor.BraceBadLight(bracePos)
        else:
            my_editor.BraceHighlight(wx.stc.STC_INVALID_POSITION, wx.stc.STC_INVALID_POSITION)
            
        event.Skip()
        
        
    def OnIdle(self, event):
        """Inactivity
        If the document has not been parsed, parse it
        """
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        profile = my_editor.GetProfile() # current user profile

        if profile.parsing.on_idle and \
           not self._checked and \
           time.time() - self._last_activity_time > 3.0:
            self.Parse()
        event.Skip()
        
        
    def OnKillFocus(self,event):
        """The editor loses the focus
        Kill any completion/calltip window opened"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        if my_editor.AutoCompActive():
            my_editor.AutoCompCancel()
        if my_editor.CallTipCancel():
            my_editor.CallTipCancel()

        event.Skip()


    def OnScroll(self,event):
        """The user scroll the text
        Kill any completion/calltip window opened"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        if my_editor.AutoCompActive():
            my_editor.AutoCompCancel()
        if my_editor.CallTipCancel():
            my_editor.CallTipCancel()
            
        event.Skip()        
            
        
    def GetObjCode(self):
        return self._objcode
