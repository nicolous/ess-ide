# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Base code manager used by the editors"""

from __future__ import with_statement

import ftplib

import common
import socket

class BaseCodeManager(object):
    """Base code manager class.
    We want to be able to change the way the code is handled (code highlight, syntax check, etc.)
    without entirely reconstruct the notebook page and reload the file.
    So this class separates the language-dependent work from the notebook page. 
    """
    
    @staticmethod 
    def GetName():
        """Returns the file format name"""
        return _("Text")
    
    @staticmethod
    def GetFormatInfo():
        """Returns a (file name prefix,file name suffix) tuple to
        known what kind of file can be opened with this editor
        """
        return ("",".txt") # accept any     
    
    @staticmethod
    def GetLogo():
        """Returns the logo of the language this editor accepts"""
        return common.TEXT_LOGO           
    
    def __init__(self, my_editor):
        """
        my_editor : Editor instance on which the changes will be perform
        """
        self._my_editor = my_editor
        
    def Start(self):
        """Called by the editor when it starts to use the code manager"""
        pass
    
    def Stop(self):
        """Called by the editor when it stops to use the code manager"""
        pass
    
    def GetMyEditor(self):
        return self._my_editor
    
    def Highlight(self, start, end):
        """Highlight the code from pos start to end
        Do nothing if not overrided"""
        pass
    
    def Parse(self):
        """Parse the code and highlight the problems
        Do nothing if not overrided"""
        pass

    def Build(self):
        """Build the code into something else
        Do nothing if not overrided"""
        pass
    
    def Comment(self):
        """Comment the current/selected line/s
        Do nothing if not overrided"""
        pass
    
    def Uncomment(self):
        """Uncomment the current/selected line/s
        Do nothing if not overrided"""
        pass    
    
    def Indent(self):
        """Indent the current/selected line/s"""
        indent_settings = self._my_editor.GetIndent()
        
        self._my_editor.BeginUndoAction()
        
        from_line = self.__myEditor.LineFromPosition(self._my_editor.GetSelectionStart())
        to_line = self.__myEditor.LineFromPosition(self._my_editor.GetSelectionEnd()) + 1
        for linenum in xrange(from_line, to_line):
            self._my_editor.SetLineIndentation(
                    linenum, self._my_editor.GetLineIndentation(linenum) + indent_settings)
            
        self._my_editor.EndUndoAction()
            
    def UnIndent(self):
        """Unindent the current/selected line/s"""
        indent_settings = self._my_editor.GetIndent()
        
        self._my_editor.BeginUndoAction()
        
        from_line = self._my_editor.LineFromPosition(self._my_editor.GetSelectionStart())
        to_line = self._my_editor.LineFromPosition(self._my_editor.GetSelectionEnd()) + 1
        for linenum in xrange(from_line, to_line):
            self._my_editor.SetLineIndentation(
                linenum, self._my_editor.GetLineIndentation(linenum) - indent_settings)
            
        self._my_editor.EndUndoAction()
    
    def FixIndent(self, start, end):
        """Fix the code indentation from start to end
        Do nothing if not overrided"""
        pass
    
    
    def ExtractBlock(self):
        """Extract the selected block for refactoring
        Do nothing of not overrided"""
        pass
    
            
    def UpdateFolding(self, start, end):
        """Update the folding information, so the user can fold blocks
        Currently never called, Do nothing if not overrided"""
        pass
    
    def SendToFtp(self, ftp_name, ftp_user, ftp_password, target_dir):
        """Send the script to a server via FTP
        ftp_name: FTP name in the user profile
        ftp_user: FTP user
        ftp_password: FTP User password
        target_dir: Destination directory where send the script
        Returns True if the operation succeed, False otherwise"""
        success = False
        profile = self._my_editor.GetProfile()
        server = None
        for ftp in profile.ftp.serverlist:
            if ftp.name == ftp_name:
                server = ftp
                break
        filepath = self._my_editor.GetFile().GetPath()
        filename = os.path.basename(filepath) 
        connect = None
        try:
            connect = ftplib.FTP(server.ip)
            if not ftp_user and not ftp_password:
                connect.login()
            else:
                connect.login(ftp_user, ftp_password)
            connect.cwd(server.gamedir)
            if target_dir and not target_dir.isspace():
                for folder in target_dir.split("/"):
                    files = []
                    try:
                        files = connect.nlst()
                    except ftplib.error_perm: # no files found
                        pass
                    if folder not in files:
                        connect.mkd(folder)
                    connect.cwd(folder)
            with open(filepath) as fileobj:
                connect.storbinary("STOR %s" % filename, fileobj)
                success = True
            connect.quit()
        except socket.error, e:
            dial = wx.MessageDialog(
                self._my_editor, " ".join([str(arg) for arg in e.args]), _("Error"), wx.ICON_ERROR)
            dial.ShowModal()
            dial.Destroy()
        except (ftplib.all_errors, IOError), e:
            dial = wx.MessageDialog(self._my_editor, str(e), _("Error"), wx.ICON_ERROR)
            dial.ShowModal()
            dial.Destroy()
        finally:
            if connect is not None:
                connect.close()
        return success
          
