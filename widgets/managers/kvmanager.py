# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import time

import wx
import wx.stc

import common
import widgets.editor as editor
import widgets.managers.manager as manager

import parsers.kv.kvpreproc as kvpreproc
import parsers.kv.kvlexer as kvlexer
import parsers.kv.kvparser as kvparser
import parsers.output.xmloutput as xmloutput

"""KeyValues source code manager classes"""

class KvCodeManager(manager.BaseCodeManager):
    """KeyValues source code manager"""
    
    # Highlight styles
    HL_NAMES = 14
    HL_BRACKET = 15
    HL_COMMENT = 16

    # Indicators
    IND_ERRORS = 0
    IND_WARNINGS = 1
    # IND_AVAILABLE = 2 # no more

    #Highlighted lex tokens
    DELIMITERS = {
        "LBRACKET" : HL_BRACKET,
        "RBRACKET" : HL_BRACKET,
        }
    
    # Marker ids
    #MK_ERROR = 0
    #MK_WARNING = 1
    
    @staticmethod 
    def GetName():
        """Returns the file format name"""
        return _("KeyValues")    
    
    @staticmethod
    def GetFormatInfo():
        """See widgets.managers.manager.BaseCodeManager"""
        return ("es_", "_db.txt")
    
    @staticmethod
    def GetLogo():
        """See widgets.managers.manager.BaseCodeManager"""
        return common.KV_LOGO
    
    def __init__(self,my_editor):
        """See widgets.managers.manager.BaseCodeManager"""
        manager.BaseCodeManager.__init__(self, my_editor)

        # Parser output
        self._parser_output = xmloutput.XmlOutput()

        # KV preprocessor
        self._preproc = kvpreproc.KvPreProcessor(self._parser_output)

        # KV lexer
        self._lexer = kvlexer.KvLexer(self._parser_output, False)

        # KV parser
        self._parser = kvparser.KvParser(self._parser_output, False, self._lexer)

        self._lexer.input("") # DOCUMENT ME: don't recall why

        # Parser result
        self._objcode = None
        
        # Last user activity (currently, last time the user pressed a key)
        self._last_activity_time = time.time()
        
        # Does the code has been parsed since the last user activity?
        self._checked = False    
        

    def Start(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        # Init the styles
        #   Names style
        my_editor.StyleSetForeground(KvCodeManager.HL_NAMES, "RED")

        #   Brackets style
        my_editor.StyleSetForeground(KvCodeManager.HL_BRACKET, "RED")
        my_editor.StyleSetBold(KvCodeManager.HL_BRACKET, True)

        #   Comment style
        my_editor.StyleSetForeground(KvCodeManager.HL_COMMENT, wx.Color(0, 128 ,0))
        my_editor.StyleSetItalic(KvCodeManager.HL_COMMENT, True)

        #   Matching braces highlight
        my_editor.StyleSetForeground(wx.stc.STC_STYLE_BRACELIGHT, "RED")
        my_editor.StyleSetBold(wx.stc.STC_STYLE_BRACELIGHT, True)
        my_editor.StyleSetBackground(wx.stc.STC_STYLE_BRACELIGHT, "YELLOW")             
        my_editor.StyleSetForeground(wx.stc.STC_STYLE_BRACEBAD, "RED")
        my_editor.StyleSetBold(wx.stc.STC_STYLE_BRACEBAD, True)
        my_editor.StyleSetBackground(wx.stc.STC_STYLE_BRACEBAD, "ORANGE")        

        # Error/Warning indicators
        my_editor.IndicatorSetStyle(KvCodeManager.IND_ERRORS,wx.stc.STC_INDIC_SQUIGGLE)
        my_editor.IndicatorSetForeground(KvCodeManager.IND_ERRORS, "RED")
        my_editor.IndicatorSetStyle(KvCodeManager.IND_WARNINGS, wx.stc.STC_INDIC_SQUIGGLE)
        my_editor.IndicatorSetForeground(KvCodeManager.IND_WARNINGS, wx.Color(255, 165, 0)) # orange
        
        # Handle text change
        my_editor.Bind(wx.stc.EVT_STC_CHARADDED, self.OnCharAdded)
        my_editor.Bind(wx.EVT_CHAR, self.OnChar)
        my_editor.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        my_editor.Bind(wx.stc.EVT_STC_UPDATEUI, self.OnUpdateUi)
        my_editor.Bind(wx.EVT_IDLE, self.OnIdle)
                        
    
    def Stop(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        my_editor.StyleClearAll()
        
        my_editor.Unbind(wx.stc.EVT_STC_CHARADDED)
        my_editor.Unbind(wx.EVT_CHAR)
        my_editor.Unbind(wx.EVT_KEY_DOWN)
        my_editor.Unbind(wx.stc.EVT_STC_UPDATEUI)
        my_editor.Unbind(wx.EVT_IDLE)                
        

    def Comment(self):
        """See widgets.managers.manager.BaseCodeManager"""    
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        my_editor.BeginUndoAction()
        
        startpos = my_editor.GetSelectionStart()
        endpos = my_editor.GetSelectionEnd()
        from_line = my_editor.LineFromPosition(startpos)
        to_line = my_editor.LineFromPosition(endpos)

        # For each line, add "//" after any leading space
        for linenum in xrange(from_line, to_line + 1):
            line = my_editor.GetLineUTF8(linenum).rstrip() # rstrip to remove \r & \n
            lineLen = len(line)
            pos = my_editor.PositionFromLine(linenum)
            i = 0
            if lineLen > 0:
                while i < lineLen and line[i].isspace():
                    i += 1
            my_editor.InsertTextUTF8(pos + i, "//")
            
        # Restore the selection
        new_startpos = my_editor.PositionFromLine(from_line)
        text = my_editor.GetLineUTF8(to_line).rstrip() # rtrip to strip \r\n
        new_endpos = my_editor.PositionFromLine(to_line) + len(text)
        my_editor.SetSelection(new_startpos, new_endpos)
        
        my_editor.EndUndoAction()
            
    def Uncomment(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        my_editor.BeginUndoAction()
        
        startpos = my_editor.GetSelectionStart()
        endpos = my_editor.GetSelectionEnd()
        from_line = my_editor.LineFromPosition(startpos)
        to_line = my_editor.LineFromPosition(endpos)

        # For each line starting with "//", remove "//"
        for linenum in xrange(from_line, to_line + 1):
            linepos = my_editor.PositionFromLine(linenum)
            text = my_editor.GetLineUTF8(linenum)
            comment_pos = text.find("//")
            if comment_pos == 0 or (comment_pos != -1 and text[:comment_pos].isspace()):
                my_editor.SetSelection(linepos + comment_pos, linepos + comment_pos + 2)
                my_editor.Clear()
          
        # Restore the selection          
        new_startpos = my_editor.PositionFromLine(from_line)
        text = my_editor.GetLineUTF8(to_line).rstrip() # rtrip to strip \r\n
        new_endpos = my_editor.PositionFromLine(to_line) + len(text)
        my_editor.SetSelection(new_startpos, new_endpos)
          
        my_editor.EndUndoAction()
          

    def Parse(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor()
        start = 0
        end = my_editor.GetTextLength()

        self._checked = True         
        
        filepath = my_editor.GetFile().GetPath()
        if filepath is None:
            filepath = "not saved"
            
        # Remove any previous error/warning
        my_editor.RemoveErrors()
        self.RemoveIndicators(start, end)            
        
        # Lex & parse it
        self._parser_output.clear()
        code = my_editor.GetText()[start:end] # code to parse (unicode to be compliant with gettext)
        source = self._preproc.parse(filepath, code)        
        objcode = self._parser.parse(filepath, source, False)
        if objcode is not None:
            self._objcode = objcode
        
        # Get/Analyze the (xml) parser output (see parsers/output/xmloutput.py)
        errordoc = self._parser_output.getDocument()
        for error in errordoc.getElementsByTagName("error"):
            line = error.getElementsByTagName("line")[0].childNodes[0].data
            message = error.getElementsByTagName("message")[0].childNodes[0].data
            my_editor.ReportError("error", line, message)
            self._HlError(int(line) - 1)
            
        for warning in errordoc.getElementsByTagName("warning"):
            line = warning.getElementsByTagName("line")[0].childNodes[0].data
            message = warning.getElementsByTagName("message")[0].childNodes[0].data
            my_editor.ReportError("warning", line, message)
            self._HlWarning(int(line) - 1)

   
    def _HlError(self, linenum):
        """Highlight an error line"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        start = my_editor.PositionFromLine(linenum)
        text = my_editor.GetLineUTF8(linenum)
        my_editor.StartStyling(start, wx.stc.STC_INDIC0_MASK)
        my_editor.SetStyling(len(text), wx.stc.STC_INDIC0_MASK)

    def _HlWarning(self, linenum):
        """Warn the user about a line"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        start = my_editor.PositionFromLine(linenum)
        text = my_editor.GetLineUTF8(linenum)
        my_editor.StartStyling(start, wx.stc.STC_INDIC1_MASK)
        my_editor.SetStyling(len(text), wx.stc.STC_INDIC1_MASK)        
        

    def RemoveIndicators(self, start, end):
        """Remove all error indicators from start to end"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance

        my_editor.StartStyling(start, wx.stc.STC_INDICS_MASK)
        my_editor.SetStyling(end, 0) # TODO: review 0      
        

    def Highlight(self, start, end):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor()
        
        code = my_editor.GetTextUTF8()[start:end]
        #code =  my_editor.GetText()[start:end].rstrip().encode("utf_8")
        
        self._lexer.input(code) # lexer.token() will return a string with the same encoding

        # While the lexer outputs tokens, do the suitable highlighting
        token = self._lexer.token()
        while token is not None:
            style = None # style to apply to this token
            
            # Does the token correspond to a delimiter?
            style = KvCodeManager.DELIMITERS.get(token.type, None)               
            if style == None:
                # Does the token correspond to a comment statement?
                if token.type == "COMMENT":
                    style = KvCodeManager.HL_COMMENT
                # Does the token correspond to a quoted text?
                elif token.type == "ID":
                        style = KvCodeManager.HL_NAMES
                else:
                    # Nothing special found, default style will be used
                    style = wx.stc.STC_STYLE_DEFAULT
                        
            # Style the code
            my_editor.StartStyling(start + token.lexpos, 0x1f)
            #  0x1f: only modify the text style bits http://www.yellowbrain.com/stc/textio.html#cells
            my_editor.SetStyling(len(token.value), style)

            token = self._lexer.token() # next token please


    def FixIndent(self, start, end):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        indent_settings = my_editor.GetIndent() # indent level setting
        
        my_editor.BeginUndoAction()
        
        linenum = start
        indent_level = 0 # indent level of the line we're reading
        while linenum < end:
            self._lexer.input(my_editor.GetLineUTF8(linenum))
            token = self._lexer.token()
            while token is not None:
                if token.type == "RBRACKET":
                    if indent_level > 0:
                        indent_level -= indent_settings
                        
                if my_editor.GetLineIndentation(linenum) != indent_level:
                    my_editor.SetLineIndentation(linenum, indent_level)
                
                if token.type == "LBRACKET":
                    indent_level += indent_settings
                    
                token = self._lexer.token()
                    
            linenum += 1
            
        my_editor.EndUndoAction()


    def Tokenize(self, text):
        """Tokenizes a text - Returns a lexer token list
        Not optimized but usefull to avoid to lex the same code in multiple functions
        """
        tokens = [] # token list to return
        
        # Lex the current line from the begin of the line to the caret position
        self._lexer.input(text)
        token = self._lexer.token()
        while token is not None:
            tokens.append(token)
            token = self._lexer.token() 
            
        return tokens         
 

    def OnCharAdded(self, event):
        """User added a char in the code
        Update the indentation if needed, and try to complete what the user is writing"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        profile = my_editor.GetProfile() # current user profile
        key = event.GetKey() # char the user added

        pos = my_editor.GetCurrentPos()
        linenum = my_editor.LineFromPosition(pos)
        currline = my_editor.GetLineUTF8(linenum)
        #currline = my_editor.GetLine(linenum).encode("utf_8")
        
        if not profile.completion.modal_only:
            # " adds or eats a "
            if chr(key) == "\"":
                if unichr(my_editor.GetCharAt(pos)) == "\"":
                    my_editor.SetSelection(pos, pos + 1)
                    my_editor.Clear()
                else:
                    my_editor.InsertTextUTF8(pos, "\"")

            # { on a new line after a key declaration adds a }
            elif chr(key) == "{":
                if currline.strip() == "{":
                    tokens = self.Tokenize(my_editor.GetLineUTF8(linenum - 1))
                    if len(tokens) == 1:
                        if tokens[0].type == "ID":
                            indent_level = my_editor.GetLineIndentation(linenum)
                                
                            my_editor.BeginUndoAction()
                            new_indent_level = indent_level + my_editor.GetIndent()
                            my_editor.NewLine()
                            my_editor.NewLine()
                            my_editor.InsertTextUTF8(pos + 2, "}")
                            my_editor.SetLineIndentation(linenum + 1, new_indent_level)
                            my_editor.GotoPos(pos + 1 + new_indent_level)
                            my_editor.SetLineIndentation(linenum + 2, indent_level)
                            my_editor.EndUndoAction()
            
                            self.Highlight(
                                pos - 1,
                                my_editor.PositionFromLine(linenum + 2) + indent_level + 1)

        # } on a new line will set the indentation to match 
        #  the indentation level of the corresponding {
        if chr(key) == "}":
            if currline.strip() == "}":
                
                wx.Yield()
                # FIXME: trick
                # Even if the char is actually added, self.BraceMatch will not find the matching brace
                # So let stc process the pending events then we can find the matching brace
                
                matching = my_editor.BraceMatch(pos - 1) # matching { position
                if matching != wx.stc.STC_INVALID_POSITION:
                    matching_line = my_editor.LineFromPosition(matching)
                    indent_level = my_editor.GetLineIndentation(matching_line)

                    if not profile.indent.modal_only:
                        my_editor.SetLineIndentation(linenum, indent_level)
                    
                    # If } is at the begin of a new line, then the user defines a new block
                    if indent_level == 0 and profile.parsing.when_relevant:
                        # Let SetLineIndentation works
                        wx.Yield()
                        # Then parse the code
                        self.Parse() 
                
        #event.Skip()
        
        
    def OnChar(self, event):
        """User wrote a char
        Remove error indicators on the current line"""
        # Note that cannot be done in onCharAdded, or there is a conflict between onStyleNeeded and the indicators removing
        
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        pos = my_editor.GetCurrentPos()
        linenum = my_editor.LineFromPosition(pos)
        text = my_editor.GetLineUTF8(linenum)
        #text = my_editor.GetLine(linenum).encode("utf_8")

        start = my_editor.PositionFromLine(linenum)
        end = start + len(text)
        self.RemoveIndicators(start, end)
        
        event.Skip()   
                    
        
    def OnKeyDown(self, event): 
        """User pressed a key
        Update the indentation if needed, show auto-completion, show calltips
        Update the last activity time from the user"""        
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        profile = my_editor.GetProfile() # current user profile
        keycode = event.GetKeyCode() # the key code the user presses
        
        pos = my_editor.GetCurrentPos()
        skip = True # if this event has to be skipped or not
        
        self._last_activity_time = time.time()  
        self._checked = False        

        # ENTER may updates the indentation, and cancel any calltip
        # ENTER after a block/event declaration auto-complete the brackets
        if keycode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER) and \
           (not my_editor.AutoCompActive()):
            # get the current line number and the text until the caret position
            linenum = my_editor.LineFromPosition(pos)
            linepos = my_editor.PositionFromLine(linenum)
            line = my_editor.GetLineUTF8(linenum).replace("\n", "").replace("\r", "")
            currline = line[:pos - linepos]

            # The indentation level stays at least the same than the previous line 
            indent_level = my_editor.GetLineIndentation(linenum)
            
            completed = False # True if we auto-complete something

            # { on a new line followed by ENTER increases the indentation
            #  (excepted if we have something else on the line)
            if currline.strip() == "{":
                # If there is no matching }, auto-complete with a new }
                if not profile.completion.modal_only and \
                   my_editor.BraceMatch(pos - 1) == wx.stc.STC_INVALID_POSITION:
                    my_editor.BeginUndoAction()
                    new_indent_level = indent_level + my_editor.GetIndent()
                    my_editor.NewLine()
                    my_editor.NewLine()
                    my_editor.InsertTextUTF8(pos + 2, "}")
                    my_editor.SetLineIndentation(linenum + 1, new_indent_level)
                    my_editor.GotoPos(pos + 1 + new_indent_level)
                    my_editor.SetLineIndentation(linenum + 2, indent_level)
                    my_editor.EndUndoAction()

                    # highlight the text we added
                    self.Highlight(
                        pos - 1,
                        my_editor.PositionFromLine(linenum + 2) + indent_level + 1)
                    
                    # new block => reparse the code
                    if profile.parsing.when_relevant:
                        self.Parse()
                        
                    completed = True
                    skip = False
                    
                # Otherwise, just update the indent level
                elif not profile.indent.modal_only:
                    new_indent_level = indent_level + my_editor.GetIndent()
                    my_editor.BeginUndoAction()
                    if my_editor.GetSelectionStart() != my_editor.GetSelectionEnd():
                        my_editor.Clear()
                        my_editor.NewLine()
                        newline = my_editor.LineFromPosition(my_editor.GetCurrentPos())
                        my_editor.SetLineIndentation(newline, new_indent_level)
                        my_editor.GotoPos(
                            my_editor.PositionFromLine(newline) + new_indent_level)
                    else:
                        my_editor.NewLine()
                        my_editor.SetLineIndentation(linenum + 1, new_indent_level)
                        my_editor.GotoPos(
                            my_editor.PositionFromLine(linenum + 1) + new_indent_level)
                    my_editor.EndUndoAction()
                    
                    completed = True
                    skip = False

            # Try to find other things to complete 
            else:
                tokens = self.Tokenize(line)
                
                if not profile.completion.modal_only:
                    # If the current line only contains 1 token auto-complete with { }
                    if len(tokens) == 1 and tokens[0].type == "ID":
                        my_editor.BeginUndoAction()

                        # Move the caret at the end of the line, so we don't break the line
                        newPos = linepos + len(line)
                        my_editor.GotoPos(newPos)
                                               
                        indent_level = my_editor.GetLineIndentation(linenum)
                        new_indent_level = indent_level + my_editor.GetIndent()

                        my_editor.NewLine()
                        my_editor.InsertTextUTF8(newPos + 1, "{")
                        my_editor.GotoPos(newPos + 2)
                        my_editor.NewLine()
                        my_editor.NewLine()
                        my_editor.InsertTextUTF8(newPos + 4, "}")
                        my_editor.SetLineIndentation(linenum + 1, indent_level)
                        my_editor.SetLineIndentation(linenum + 2, new_indent_level)
                        my_editor.SetLineIndentation(linenum + 3, indent_level)
                        
                        newLinePos = my_editor.PositionFromLine(linenum + 2)
                        my_editor.GotoPos(newLinePos + new_indent_level)
                        my_editor.EndUndoAction()
                        
                        # Highlight the code we added
                        self.Highlight(
                            pos, my_editor.PositionFromLine(linenum + 3) + indent_level + 1)
                        
                        # New block => reparse the code
                        if profile.parsing.when_relevant:
                            self.Parse()
                        
                        completed = True
                        skip = False
                                            
            
            # If we didn't find anything to auto-complete, just maintain the current indent level
            if not completed and not profile.indent.modal_only:
                my_editor.BeginUndoAction()
                if my_editor.GetSelectionStart() != my_editor.GetSelectionEnd():
                    my_editor.Clear()
                    my_editor.NewLine()
                    newline = my_editor.LineFromPosition(my_editor.GetCurrentPos())
                    my_editor.SetLineIndentation(newline,indent_level)
                    my_editor.GotoPos(my_editor.PositionFromLine(newline) + indent_level)
                else:
                    my_editor.NewLine()
                    my_editor.SetLineIndentation(linenum + 1,indent_level)
                    my_editor.GotoPos(my_editor.PositionFromLine(linenum + 1) + indent_level)
                my_editor.EndUndoAction()
                
                skip = False

            # Cancel any calltip
            if my_editor.CallTipCancel():
                my_editor.CallTipCancel()

        # BACK may auto-remove some additional chars to help
        elif keycode == wx.WXK_BACK:
            linenum = my_editor.LineFromPosition(pos)
            
            # Remove ""
            if my_editor.GetTextRangeUTF8(pos - 1, pos + 1) == "\"\"":
                my_editor.SetSelection(pos, pos + 1)
                my_editor.Clear()
                
        if skip:
            event.Skip()


    def OnUpdateUi(self, event):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        pos = my_editor.GetCurrentPos()

        brace_pos = wx.stc.STC_INVALID_POSITION # matching bracket position

        # Is there a bracket before the caret?
        char = my_editor.GetCharAt(pos - 1)
        if char > 0 and chr(char) in "{}":
            if my_editor.GetStyleAt(pos - 1) == KvCodeManager.HL_BRACKET:
                brace_pos = pos - 1
        else:
            # ...or maybe after?
            char = my_editor.GetCharAt(pos)
            if char > 0 and chr(char) in "{}":
                if my_editor.GetStyleAt(pos) == KvCodeManager.HL_BRACKET:
                    brace_pos = pos
        
        # Ok, there is a bracket near the caret and we know its position
        # Highlight any matching bracket 
        if brace_pos != wx.stc.STC_INVALID_POSITION:
            # Search for matching bracket
            match = my_editor.BraceMatch(brace_pos)
            
            # Highlight
            if match != wx.stc.STC_INVALID_POSITION:
                my_editor.BraceHighlight(brace_pos,match)
            #else:
            #    my_editor.BraceBadLight(brace_pos)
        else:
            my_editor.BraceHighlight(
                wx.stc.STC_INVALID_POSITION, wx.stc.STC_INVALID_POSITION)
            
        event.Skip()
        
        
    def OnIdle(self, event):
        """Inactivity
        If the document has not been parsed, parse it
        """
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        profile = my_editor.GetProfile() # current user profile

        if profile.parsing.on_idle and not self._checked and \
           time.time() - self._last_activity_time > 3.0:
            self.Parse()
        event.Skip()
        
        
    def GetObjCode(self):
        return self._objcode
