# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

import keyword

import wx
import wx.stc

import common
import widgets.managers.manager as manager

"""Python source code manager classes"""

class PyCodeManager(manager.BaseCodeManager):
    """Python source code manager"""
    
    @staticmethod 
    def GetName():
        """Returns the file format name"""
        return _("Python")    
    
    @staticmethod
    def GetFormatInfo():
        """See widgets.managers.manager.BaseCodeManager"""
        return ("", ".py")
    
    @staticmethod
    def GetLogo():
        """See widgets.managers.manager.BaseCodeManager"""
        return common.PY_LOGO  

    def Start(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        #my_editor.SetReadOnly(True)
        
        # Set the editor to use the Python lexer
        my_editor.SetLexer(wx.stc.STC_LEX_PYTHON)
        my_editor.SetKeyWords(0, " ".join(keyword.kwlist))

        # Global default styles for all languages
        my_editor.StyleSetSpec(wx.stc.STC_STYLE_BRACELIGHT, "fore:WHITE,back:BLUE,bold")
        my_editor.StyleSetSpec(wx.stc.STC_STYLE_BRACEBAD, "fore:BLACK,back:RED,bold")

        # Python styles
        # Default 
        my_editor.StyleSetSpec(wx.stc.STC_P_DEFAULT, "fore:BLACK")
        # Comments
        my_editor.StyleSetSpec(wx.stc.STC_P_COMMENTLINE, "fore:DARK GREY")
        # Number
        my_editor.StyleSetSpec(wx.stc.STC_P_NUMBER, "fore:ORANGE")
        # String
        my_editor.StyleSetSpec(wx.stc.STC_P_STRING, "fore:BLUE")
        # Single quoted string
        my_editor.StyleSetSpec(wx.stc.STC_P_CHARACTER, "fore:BLUE")
        # Keyword
        my_editor.StyleSetSpec(wx.stc.STC_P_WORD, "fore:ORANGE,bold")
        # Triple quotes
        my_editor.StyleSetSpec(wx.stc.STC_P_TRIPLE, "fore:BLUE")
        # Triple double quotes
        my_editor.StyleSetSpec(wx.stc.STC_P_TRIPLEDOUBLE, "fore:BLUE")
        # Class name definition
        my_editor.StyleSetSpec(wx.stc.STC_P_CLASSNAME, "fore:BLACK")
        # Function or method name definition
        my_editor.StyleSetSpec(wx.stc.STC_P_DEFNAME, "fore:BLACK")
        # Operators
        my_editor.StyleSetSpec(wx.stc.STC_P_OPERATOR, "fore:GREEN")
        # Identifiers
        my_editor.StyleSetSpec(wx.stc.STC_P_IDENTIFIER, "fore:BLACK")
        # Comment-blocks
        my_editor.StyleSetSpec(wx.stc.STC_P_COMMENTBLOCK, "fore:DARK GREY")
        # End of line where string is not closed
        my_editor.StyleSetSpec(wx.stc.STC_P_STRINGEOL, "fore:BLACK,back:THISTLE,eol")
        
        # Handle text change       
        my_editor.Bind(wx.stc.EVT_STC_CHARADDED, self.OnCharAdded)
        my_editor.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
                        
    
    def Stop(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        my_editor.StyleClearAll()
        
        my_editor.Unbind(wx.stc.EVT_STC_CHARADDED)        
        my_editor.Unbind(wx.EVT_KEY_DOWN)
 
 
    def Comment(self):
        """See widgets.managers.manager.BaseCodeManager"""    
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        my_editor.BeginUndoAction()
        
        startpos = my_editor.GetSelectionStart()
        endpos = my_editor.GetSelectionEnd()
        from_line = my_editor.LineFromPosition(startpos)
        to_line = my_editor.LineFromPosition(endpos)

        # For each line, add "#" after any leading space
        for linenum in xrange(from_line, to_line + 1):
            line = my_editor.GetLineUTF8(linenum).rstrip() # rstrip to remove \r & \n
            lineLen = len(line)
            pos = my_editor.PositionFromLine(linenum)
            i = 0
            if lineLen > 0:
                while i < lineLen and line[i].isspace():
                    i += 1
            my_editor.InsertTextUTF8(pos + i, "#")
            
        # Restore the selection
        new_startpos = my_editor.PositionFromLine(from_line)
        text = my_editor.GetLineUTF8(to_line).rstrip() # rtrip to strip \r\n
        new_endpos = my_editor.PositionFromLine(to_line) + len(text)
        my_editor.SetSelection(new_startpos, new_endpos)
        
        my_editor.EndUndoAction()
            
    def Uncomment(self):
        """See widgets.managers.manager.BaseCodeManager"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        
        my_editor.BeginUndoAction()
        
        startpos = my_editor.GetSelectionStart()
        endpos = my_editor.GetSelectionEnd()
        from_line = my_editor.LineFromPosition(startpos)
        to_line = my_editor.LineFromPosition(endpos)

        # For each line starting with "#", remove "#"
        for linenum in xrange(from_line, to_line + 1):
            linepos = my_editor.PositionFromLine(linenum)
            text = my_editor.GetLineUTF8(linenum)
            comment_pos = text.find("#")
            if comment_pos == 0 or (comment_pos != -1 and text[:comment_pos].isspace()):
                my_editor.SetSelection(linepos + comment_pos, linepos + comment_pos + 2)
                my_editor.Clear()
          
        # Restore the selection          
        new_startpos = my_editor.PositionFromLine(from_line)
        text = my_editor.GetLineUTF8(to_line).rstrip() # rtrip to strip \r\n
        new_endpos = my_editor.PositionFromLine(to_line) + len(text)
        my_editor.SetSelection(new_startpos, new_endpos)
          
        my_editor.EndUndoAction()
        
        
    def OnCharAdded(self, event):
        """User added a char in the code
        Update the indentation if needed, and try to complete what the user is writing"""
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        profile = my_editor.GetProfile() # current user profile
        key = event.GetKey() # char the user added

        pos = my_editor.GetCurrentPos()
        linenum = my_editor.LineFromPosition(pos)
        currline = my_editor.GetLineUTF8(linenum)
        #currline = my_editor.GetLine(linenum).encode("utf_8")
        
        if not profile.completion.modal_only:
            # " adds or eats a "
            if chr(key) == "\"":
                if unichr(my_editor.GetCharAt(pos)) == "\"":
                    my_editor.SetSelection(pos, pos + 1)
                    my_editor.Clear()
                else:
                    my_editor.InsertTextUTF8(pos, "\"")

        #event.Skip()        
            
        
    def OnKeyDown(self, event): 
        """User pressed a key
        Update the indentation if needed, show auto-completion, show calltips
        Update the last activity time from the user"""        
        my_editor = self.GetMyEditor() # StyledTextCtrl instance
        profile = my_editor.GetProfile() # current user profile
        keycode = event.GetKeyCode() # the key code the user presses
        
        pos = my_editor.GetCurrentPos()
        skip = True # if this event has to be skipped or not
        
        # ENTER may updates the indentation, and cancel any calltip
        # ENTER after a block/event declaration auto-complete the brackets
        if keycode in (wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER) and \
           (not my_editor.AutoCompActive()):
            # get the current line number and the text until the caret position
            linenum = my_editor.LineFromPosition(pos)
            linepos = my_editor.PositionFromLine(linenum)
            line = my_editor.GetLineUTF8(linenum).replace("\n", "").replace("\r", "")
            currline = line[:pos - linepos]

            # The indentation level stays at least the same than the previous line 
            indent_level = my_editor.GetLineIndentation(linenum)
            
            completed = False # True if we auto-complete something            
            
            # { on a new line followed by ENTER increases the indentation
            #  (excepted if we have something else on the line)
            if currline.endswith(":"):
                if not profile.completion.modal_only:
                    my_editor.BeginUndoAction()
                    new_indent_level = indent_level + my_editor.GetIndent()
                    my_editor.NewLine()
                    my_editor.SetLineIndentation(linenum + 1, new_indent_level)
                    my_editor.GotoPos(pos + 1 + new_indent_level)
                    my_editor.EndUndoAction()

                    completed = True
                    skip = False

            # If we didn't find anything to auto-complete, just maintain the current indent level
            if not completed and not profile.indent.modal_only:
                my_editor.BeginUndoAction()
                if my_editor.GetSelectionStart() != my_editor.GetSelectionEnd():
                    my_editor.Clear()
                    my_editor.NewLine()
                    newline = my_editor.LineFromPosition(my_editor.GetCurrentPos())
                    my_editor.SetLineIndentation(newline,indent_level)
                    my_editor.GotoPos(my_editor.PositionFromLine(newline) + indent_level)
                else:
                    my_editor.NewLine()
                    my_editor.SetLineIndentation(linenum + 1,indent_level)
                    my_editor.GotoPos(my_editor.PositionFromLine(linenum + 1) + indent_level)
                my_editor.EndUndoAction()
                
                completed = True                
                skip = False

        # BACK may auto-remove some additional chars to help
        elif keycode == wx.WXK_BACK:
            linenum = my_editor.LineFromPosition(pos)
            
            # Remove ""
            if my_editor.GetTextRangeUTF8(pos - 1, pos + 1) == "\"\"":
                my_editor.SetSelection(pos, pos + 1)
                my_editor.Clear()
                
        if skip:
            event.Skip()
   