# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Set of source code editors stored in a notebook control"""

import wx
import wx.stc
import wx.lib.newevent

import unicodedata
import os
import stat

import common
import data.file as file
import data.profiles as profiles
import widgets.managers.manager as manager

# Custom event: Thrown to the main frame when the user selects or closes a notebook page
EditorTabsEvent, EVT_EDITORTABS_PAGE_CHANGED = wx.lib.newevent.NewEvent()


class EditorTabs(wx.Notebook):
    """Text editor tabs"""
    
    def __init__(self, frame, *args, **kwargs):
        """
        frame: Main frame instance
        """
        wx.Notebook.__init__(self, *args, **kwargs)
        self._frame = frame
        
        # Subscribe to the needed events (see the callbacks for more info)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnPageChange)
        self.Bind(wx.EVT_RIGHT_UP, self.OnRightClickUp)        

        # Current tab selected (see EditorsTabs.GetSelection)
        self._selection = -1

        # Context menu shown when the user right-click on a tab
        self._context_menu = wx.Menu()
        menu_close_item = self._context_menu.Append(wx.ID_CLOSE, "&Close", "Close Page")
        self.Bind(wx.EVT_MENU, self.OnContextMenuClose, menu_close_item)

        
    def OnPageChange(self, event):
        """The user selected a new page
        Update the last selection var then throw an event to the main frame"""
        self._selection = event.GetSelection()
        wx.PostEvent(self._frame, EditorTabsEvent())
        event.Skip() # http://wiki.wxpython.org/wxPython%20Platform%20Inconsistencies#Switching_pages_messing_up_rendering_under_MS_Windows


    def OnRightClickUp(self, event):
        """The user right-clicked on a tab
        Pop-up a context menu"""
        position = event.GetPosition()
        pagenum = self.HitTest(position)[0]
        
        if pagenum != wx.NOT_FOUND: 
            self.SetSelection(pagenum)
            self.PopupMenu(self._context_menu, position)
        

    def OnContextMenuClose(self, event):
        """The user chosen to close a tab in the context menu (see EditorTabs.onRightClickUp)
        Close the tab and send an event to the main frame"""
        page = self.GetCurrentPage()
        if page.Close():
            wx.PostEvent(self._frame, EditorTabsEvent())        
        
     
    def CloseAll(self):
        """Close all tabs/files
        Returns False if one or more files haven't been closed"""
        all_closed = True
        i = self.GetPageCount()-1
        while i >= 0:
            page = self.GetPage(i)
            if not page.Close():
                all_closed &= False
            i -= 1
        wx.PostEvent(self._frame, EditorTabsEvent())             
        return all_closed
        

    def GetSelection(self):
        """We want self.GetSelection to be correctly updated if the user closes a tab
        See EditorTabs.onPageChange"""
        return self._selection
        # http://wiki.wxpython.org/wxPython%20Platform%20Inconsistencies#EVT_NOTEBOOK_PAGE_CHANGED_not_created_on_wxNotebook.DeletePage_using_wxPython_with_GTK_1.2 


    def ChangeSelection(self, selection):
        """See EditorTabs.GetSelection"""
        wx.Notebook.ChangeSelection(self, selection)
        self._selection = selection
        
        if self.GetPageCount() != 1:
            # because wx.Notebook.ChangeSelection does not fire EVT_NOTEBOOK_PAGE_CHANGED if there is more than one tab? or not yet?
            wx.PostEvent(self._frame, EditorTabsEvent())         
     

    def NewPage(self, title, ManagerType = None, filepath = None):
        """Open a new editor
        title: Page title
        managerType : Code manager type used by the editor (can be None)
        filepath: File path (can be None)"""
        pagecount = self.GetPageCount()
        
        # If the file has a path, check if it is already opened
        # If yes, it will be selected instead of be opened again
        already_opened = False
        if filepath is not None:
            i = pagecount - 1
            while i >= 0:
                page = self.GetPage(i)
                f = page.GetFile() 
                if f is not None and f.GetPath() == filepath:
                    already_opened = True
                    break
                i -= 1    
            
            if i != -1:
                self.ChangeSelection(i)
        
        if not already_opened: # still False if filepath is None
            # Guess the best editor to open this file
            if filepath is not None and ManagerType is None:
                ManagerType = Editor.GetManagerType(os.path.basename(filepath))
                                        
            # Create/Add/Select a new page            
            textctrl = Editor(self._frame, pagecount, self)
            if ManagerType != manager.BaseCodeManager:
                textctrl.SetCodeManager(ManagerType(textctrl))
            self.AddPage(textctrl, title)
            self.ChangeSelection(pagecount)

            # Load the file
            if filepath is not None:
                try:
                    textctrl.Load(filepath)
                except IOError, e:
                    dialog = wx.MessageDialog(
                                self._frame,
                                _("Unable to read the file: %(error)s") % {"error" : e},
                                _("I/O Error"),
                                 wx.ICON_ERROR | wx.OK) 
                    dialog.ShowModal()
                    dialog.Destroy()
                    textctrl.Close()

class Editor(wx.stc.StyledTextCtrl):     
    """Base source code editor"""
    
    _next_id = 1
    @staticmethod
    def _GenNextId():
        current_id = str(Editor._next_id)
        Editor._next_id += 1
        return str(current_id)

    _code_manager_types = []
    @staticmethod
    def RegisterManagerType(classtype):
        """Register a new code manager type"""
        Editor._code_manager_types.append(classtype)
        
    @staticmethod
    def GetManagerTypes():
        """Returns a list of all code manager types"""
        return Editor._code_manager_types
    
    @staticmethod
    def GetManagerType(filename):
        """Returns a code manager type that accepts this kind of file"""
        for Manager in Editor.GetManagerTypes():
            format = Manager.GetFormatInfo()
            if filename.startswith(format[0]) and filename.endswith(format[1]):
                return Manager
        return manager.BaseCodeManager
    

    def __init__(self, frame, book_position, *args, **kwargs):
        """
        frame: Main application frame
        book_position: Position in the notebook
        """
        wx.stc.StyledTextCtrl.__init__(self, *args, **kwargs)

        self._frame = frame
        self._book_position = book_position
        
        # Unique file id & file instance
        self._id = Editor._GenNextId()
        self._file = file.File()
        
        # Source code manager
        self._code_manager = manager.BaseCodeManager(self)
        
        # Default user profile
        # TODO: Move this when projects will be supported
        self._profile = profiles.Profile("default")
        
        # Last file modification time on the disk
        self._mtime = 0 
        
        # Make a first save point (text marked as non-modified)
        self.SetSavePoint()
        self.EmptyUndoBuffer()
        self._modified = False
        # A file considered as modified satisfies at least one of these criteria:
        # - GetModify returns True (more onSavePointLeft fired than onSavePointReached)
        # - self._modified has been set to True        
        
        # Removes unnecessary scroll bars
        self.SetScrollWidth(1)
                
        # Set tabs/indentation properties
        self.SetUseTabs(0) # tabs are spaces
        self.SetTabWidth(4)
        self.SetIndentationGuides(1) # indent level indicator
        self.SetIndent(4) # 1 indent level is 4 spaces
        #self.SetBackSpaceUnIndents(1) # backspace unindent      

        #self.SetHighlightGuide(1)

        # Display line numbers
        self.SetMarginType(0, wx.stc.STC_MARGIN_NUMBER)
        #self.SetMarginWidth(0,10) # EssEditor.updateLineNumbers() will update this
        self.UpdateLineNumbers()
        
        # Hide unused margins
        self.SetMarginWidth(2, 0)
        
        # Customize the caret
        self.SetCaretLineBack(wx.Color(240, 240, 240))
        self.SetCaretLineVisible(1)

        # REVIEW ME: LF eol seems to write CRLF :-s
        # In addition, this is the unique eol mode which
        # does not add an extra CR under Windows when copying/pasting
        self.SetEOLMode(wx.stc.STC_EOL_LF)
        
        #   Default text style
        # http://wxwidgets2.8.sourcearchive.com/documentation/2.8.7.1/wxPython_2stc_8py-source.html
        # font = wx.Font(9,wx.FONTFAMILY_MODERN,wx.FONTSTYLE_NORMAL,wx.FONTWEIGHT_NORMAL,False,"Courier New",common.IDE_CODECS_FONTCODING[self.__encoding])
        font = wx.Font(
                    self._profile.font.size,
                    wx.FONTFAMILY_MODERN,
                    wx.FONTSTYLE_NORMAL,
                    wx.FONTWEIGHT_NORMAL,
                    False,
                    self._profile.font.name)
        self.StyleSetFont(wx.stc.STC_STYLE_DEFAULT, font)
        '''if self._encoding == "ascii":
            self.StyleSetCharacterSet(wx.stc.STC_STYLE_DEFAULT, wx.stc.STC_CHARSET_ANSI)
        else:
            self.StyleSetCharacterSet(wx.stc.STC_STYLE_DEFAULT, wx.stc.STC_CHARSET_DEFAULT)
        if self._encoding == "utf_8":        
            self.StyleSetFontEncoding(wx.stc.STC_STYLE_DEFAULT, wx.FONTENCODING_UTF8)'''
        self.StyleClearAll() # default style applied to all the styles        
        
        # Folding margin (stub)
        self.SetMarginType(1, wx.stc.STC_MARGIN_SYMBOL)
        self.SetMarginMask(1, wx.stc.STC_MASK_FOLDERS)
        self.SetMarginWidth(1, 16)
        self.MarkerDefine(
            wx.stc.STC_MARKNUM_FOLDEREND,
            wx.stc.STC_MARK_BOXPLUSCONNECTED,
            "white",
            "black")
        self.MarkerDefine(
            wx.stc.STC_MARKNUM_FOLDEROPENMID,
            wx.stc.STC_MARK_BOXMINUSCONNECTED,
            "white",
            "black")
        self.MarkerDefine(
            wx.stc.STC_MARKNUM_FOLDERMIDTAIL,
            wx.stc.STC_MARK_TCORNER,
            "white",
            "black")
        self.MarkerDefine(
            wx.stc.STC_MARKNUM_FOLDERTAIL,
            wx.stc.STC_MARK_LCORNER,
            "white",
            "black")
        self.MarkerDefine(
            wx.stc.STC_MARKNUM_FOLDERSUB,
            wx.stc.STC_MARK_VLINE,
            "white",
            "black")
        self.MarkerDefine(
            wx.stc.STC_MARKNUM_FOLDER,
            wx.stc.STC_MARK_BOXPLUS,
            "white",
            "black")
        self.MarkerDefine(
            wx.stc.STC_MARKNUM_FOLDEROPEN,
            wx.stc.STC_MARK_BOXMINUS,
            "white",
            "black")
        
        
        # Auto-completion settings
        self.AutoCompSetDropRestOfWord(True)
        self.AutoCompSetIgnoreCase(True)
        
        # Subscribe to some events
        self.SetLexer(wx.stc.STC_LEX_CONTAINER)
        self.Bind(wx.stc.EVT_STC_STYLENEEDED, self.OnStyleNeeded)        
        self.Bind(wx.stc.EVT_STC_SAVEPOINTLEFT, self.OnSavePointLeft)
        self.Bind(wx.stc.EVT_STC_SAVEPOINTREACHED, self.OnSavePointReached)
        self.Bind(wx.stc.EVT_STC_CHANGE, self.OnChange)
        self.Bind(wx.EVT_SET_FOCUS, self.OnSetFocus)
        
        
    def UpdateLineNumbers(self):
        """Update the line numbers margin width"""
        self.SetMarginWidth(0, 10 * len(str(self.GetLineCount())))    
        
        
    def Load(self, filepath):
        """Load text from a file"""
        self._file.SetPath(filepath)        
        text = self._file.Load()

        # Update the last modification time
        self._mtime = os.stat(filepath)[stat.ST_MTIME]
        
        #if wx.PlatformInfo[2] == "unicode":
        #self.SetCodePage(wx.stc.STC_CP_UTF8)
    
        readonly = self.GetReadOnly()
        self.SetReadOnly(False)
        # http://arnout.engelen.eu/~wxwidgets/xmldocs/applications/texdocs/wxWidgets/wxPython/docs/CHANGES.html#id3 
        #     => new UTF8 methods
        #if self._encoding == "utf_8":
        self.SetText(text) # <=> self.SetTextUTF8(text.encode("utf_8"))
        #else:
        #    #self.SetText(text.encode(self._encoding))
        #    self.SetTextUTF8(text.encode("utf_8"))
        #    #self.SetText(text)
        self.SetReadOnly(readonly)
       
        # Make a first save point (text marked as non-modified)
        self.SetSavePoint()
        self.EmptyUndoBuffer()
        #self.SetModified(False)
        
        # Update folding info
        #self.UpdateFolding(0, textLen)        
        # Update the line number margin
        self.UpdateLineNumbers()

        # Highlight and parse the code
        text_len = self.GetTextLength() # don't use len(text), or convert text in unicode
        wx.Yield() # let stc displays the text before highlight/parse (avoid some graphical oddness)
        self._code_manager.Highlight(0, text_len)
        if self._profile.parsing.on_open_file:
            self._code_manager.Parse()        
        
    def Reload(self):
        """Reload the file from the disk
        Plus try to restore the view"""
        pos = self.GetCurrentPos()
        linenum = self.LineFromPosition(pos)
        
        self.Load(self._file.GetPath())
        self._frame.UpdateFileButtons()
        
        size = self.GetTextLength()
        linecount = self.GetLineCount()
        if pos <= size:
            self.GotoPos(pos)
        else:
            self.GotoPos(size)
        if linenum <= linecount:
            self.ScrollToLine(linenum)
        else:
            self.ScrollToLine(linecount)
    
    def Save(self, force_save_as = False):
        """Save the file
        force_save_as: If True, show a file dialog
        Returns False if the save has been aborted"""
        saved = False
        filepath = self._file.GetPath()
        save_as = force_save_as or (filepath is None)
        last_opened_dir = self._frame.GetLastOpenedDir()
        
        # Show a file dialog if required
        if save_as:
            format_info = type(self._code_manager).GetFormatInfo()
            suggestion = "%snewfile%s" % (format_info[0], format_info[1]) # default file name
            if format_info[0] or format_info[1]:
                visibility = "%s*%s" % (format_info[0], format_info[1]) # default filter
            else:
                visibility = "*.*" 
            
            diag = wx.FileDialog(
                        self._frame,
                        _("Save As"),
                        last_opened_dir,
                        suggestion,
                        visibility,
                        wx.SAVE | wx.FD_OVERWRITE_PROMPT)

            if diag.ShowModal() == wx.ID_OK:
                filename = diag.GetFilename()
                last_opened_dir = diag.GetDirectory()
                self._frame.SetLastOpenedDir(last_opened_dir)
                filepath = "%s/%s" % (last_opened_dir, filename)
                self._file.SetPath(filepath)
                
                # Update the tab name
                tabs = self.GetParent()
                pagenum = tabs.GetSelection()
                tabs.SetPageText(pagenum, os.path.basename(filepath))                
            diag.Destroy()            

        if filepath is not None:        
            # Try to write the file
            bom, encoding = self._file.GetEncoding()
            try:
                data = self.GetText().encode(encoding)
                 
                self._file.Save(data)
                saved = True
            except IOError, e:
                diag = wx.MessageDialog(
                            self._frame,
                            _("Unable to write the file: %(error)s") % {"error" : e},
                            "I/O Error",
                            wx.ICON_ERROR | wx.OK)
                diag.ShowModal()
                diag.Destroy()
            except UnicodeEncodeError:
                # Can't encode all characters
                # Ask the user to convert in utf-8
                diag = wx.MessageDialog(
                            self._frame,
                            _("Some characters cannot be properly encoded in %(encoding)s\nDo you prefer to encode the file utf_8?")
                                % {"encoding" : encoding},
                            _("Warning"),
                            wx.ICON_EXCLAMATION | wx.YES_NO | wx.CANCEL)
                # TODO: custom dialog
                res = diag.ShowModal()

                if res == wx.ID_YES:
                    # Encode to utf-8
                    encoding = "utf_8"
                    self._file.SetEncoding(None, encoding)
                    data = self.GetText().encode(encoding)
                    
                    self._file.Save(data)
                    saved = True
                elif res == wx.ID_NO:
                    # Convert non-ascii characters
                    data = unicodedata.normalize("NFKD", self.GetText()).encode(encoding, "ignore")
                    self.SetText(data) # self.SetTextUTF8(newText.encode("utf_8"))
                    self._code_manager.Highlight(0, len(data))
                    
                    self._file.Save(data)
                    saved = True
                diag.Destroy()
                
        if saved:
            # Update the last modification time
            self._mtime = os.stat(filepath)[stat.ST_MTIME]
            
            # If the file was "saved as", now that the file has a name, we could find the suitable source code manager to use
            if save_as:
                filename = os.path.basename(filepath)
                ManagerType = Editor.GetManagerType(filename)
                if ManagerType != type(self._code_manager):
                    self.SetCodeManager(ManagerType(self))
            
            # Parse the code
            self._code_manager.Parse()
                    
            # Mark the file as unmodified
            self.SetSavePoint()
            self._modified = False
            
        #saved = True
        return saved
    
    def Close(self):
        """Close this editor
        Returns False if aborted by the user
        """
        can_be_closed = False        
        tabs = self.GetParent()
        selection = tabs.GetSelection()
        modified = self.IsModified()

        if modified:
            message = _("Save file?")
            filepath = self._file.GetPath()
            if filepath is not None:
                message = _("Save file \"%(filepath)s\"?") % {"filepath" : filepath}

            dial = wx.MessageDialog(
                        self._frame,
                        message,
                        _("Save"),
                        wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)
            result = dial.ShowModal()
            if result == wx.ID_YES:
                if self.Save():
                    can_be_closed = True
            elif result == wx.ID_NO:
                can_be_closed = True
            dial.Destroy()
        else:
            can_be_closed = True
            
        if can_be_closed:            
            # Remove any error related to this file from the error list
            # TODO: Don't do that if the file is owned by a project            
            self.RemoveErrors()                       
                
            # Close the page
            tabs.DeletePage(selection)                  

        return can_be_closed 
            

    def SetModified(self, modified):
        """Warn the user that the text has been modified
        Change the name of the tab page"""
        tabs = self.GetParent()
        pagenum = tabs.GetSelection()
        
        # The ctrl could not yet being in the notebook (and that's normal)
        if pagenum != -1: 
            text = tabs.GetPageText(pagenum)
            if modified:
                if not text.startswith("*"):
                    tabs.SetPageText(pagenum, "*%s" % tabs.GetPageText(pagenum))
            else:        
                if text.startswith("*"):
                    tabs.SetPageText(pagenum,text[1:])
            self._modified = modified
            self._frame.UpdateFileButtons()

            
    def SetFileEncoding(self, new_encoding):
        """Set the encoding used to save the code"""
        bom, encoding = self._file.GetEncoding()       
        if encoding != new_encoding:
            self._modified = True
            self.SetModified(True)
            bom = None # bom confuses ES about the first script line, this IDE will not take part
            encoding = new_encoding
            self._file.SetEncoding(bom, encoding)                
                

    def ReportError(self, errtype, line, description):
        """Add an error to the error list"""
        errorlist = self._frame.GetToolsTabs().GetErrorList()
        errorlist.AddError(self, errtype, line, description)
        
    def RemoveErrors(self):
        """Remove all errors reported from the error list"""
        errorlist = self._frame.GetToolsTabs().GetErrorList()
        errorlist.RemoveErrors(self)
        

    def IsModified(self):
        """Is the file modified?"""
        return self._modified or self.GetModify() 


    def CheckExternalModif(self):
        """Check for external modification
        Returns True if an external modification has been detected
        False otherwise"""
        modified = False
        
        filepath = self._file.GetPath()
        if filepath is not None:
            if not os.path.isfile(filepath):
                self.SetModified(True)
                self._file.SetPath(None)
            
                dial = wx.MessageDialog(
                            self._frame,
                            _("The file has been deleted by another program."),
                            _("Deletion"),
                            wx.ICON_WARNING)
                dial.ShowModal()
                dial.Destroy()
                
            elif os.stat(filepath)[stat.ST_MTIME] != self._mtime:
                self.SetModified(True)
                self._mtime = os.stat(filepath)[stat.ST_MTIME]
                
                dial = wx.MessageDialog(
                            self._frame,
                            _("The file has been modified by another program. Reload it?"),
                            _("Reload"),
                            wx.YES_NO | wx.YES_DEFAULT | wx.ICON_WARNING)
                if dial.ShowModal() == wx.ID_YES:
                    self.Reload()
                dial.Destroy()    
                
        return modified

    def OnStyleNeeded(self, event):
        """STC considers that some text needs to be styled
        Highlight what needs style"""        
        begin_pos = self.GetEndStyled()
        end_pos = self.GetCurrentPos()
        
        begin_linenum = self.LineFromPosition(begin_pos)
        end_linenum = self.LineFromPosition(end_pos)
        
        start = self.PositionFromLine(begin_linenum)
        end = self.PositionFromLine(end_linenum + 1)

        self._code_manager.Highlight(start, end)
        event.Skip()

    def OnSavePointLeft(self, event):
        """User made a change since the last save point
        Change the page name to indicate that the file needs to be saved"""        
#        print "left"
        self.SetModified(True)
        event.Skip()

    def OnSavePointReached(self, event):
        """Text entered/returned to an unmodified state
        Change the page name to remove any indicator about changes that need to be saved"""        
#        print "reached"
        self.SetModified(False)
        event.Skip()            
        
    def OnChange(self, event):
        """Something changed in the text
        Update the line numbers
        """
        self.UpdateLineNumbers()

    def OnSetFocus(self, event):
        """The window has the focus
        Check for external modification of our file
        """
        self.CheckExternalModif()
        event.Skip()


    def GetFrame(self):
        return self._frame
        
    def GetFile(self):
        return self._file
    
    def GetFrame(self):
        return self._frame
        
    def GetBookPosition(self):
        return self._book_position
        
    def SetBookPosition(self, n):
        self._book_position = n
        
    def GetFileId(self):
        return self._id    
    
    def SetCodeManager(self, manager):
        """Modify the code manager used by this editor"""
        self._code_manager.Stop()
        self._code_manager = manager
        self._code_manager.Start()
        
        text_len = self.GetTextLength() # don't use len(text), or convert text in unicode
        self._code_manager.Highlight(0, text_len)        
        
    def GetCodeManager(self):
        return self._code_manager    
        
    def GetProfile(self):
        return self._profile
    
