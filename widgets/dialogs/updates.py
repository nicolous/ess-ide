# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Update related dialogs"""

import os
import sys

import wx

import common

class UpdateDialog(wx.Dialog):
    """Show update details"""
    
    def __init__(self,version, *args, **kwargs):
        wx.Dialog.__init__(self, *args, **kwargs)
        
        panel = wx.Panel(self)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sub_sizer = wx.BoxSizer(wx.VERTICAL)
        
        # Update available!
        # Your version: blah
        # Last version: blah
        # [Download the last version]
        info_panel = wx.Panel(panel)
        info_sizer = wx.BoxSizer(wx.VERTICAL)
        logo = wx.Image(
                    os.path.join(os.path.dirname(sys.argv[0]), common.IDE_MASCOT),
                    wx.BITMAP_TYPE_PNG
                    ).ConvertToBitmap()
                    
        info_sizer.Add(
            wx.StaticBitmap(parent=info_panel, bitmap=logo),
            0,
            wx.BOTTOM | wx.ALIGN_CENTER_HORIZONTAL,
            5)
        
        #info_sizer.Add(wx.StaticText(info_panel,wx.ID_ANY,"Update available!"))
        info_sizer.Add(
            wx.StaticText(
                parent=info_panel,
                label=_("Your version: %(version)s") % {"version" : common.IDE_VERSION}))
        
        info_sizer.Add(
            wx.StaticText(
                parent=info_panel,
                label=_("Last version: %(version)s") % {"version" : version}))
        
        info_sizer.Add(
            wx.HyperlinkCtrl(
                parent=info_panel,
                id=wx.ID_ANY,
                label=_("Visit the website"),
                url=common.IDE_WEBSITE))
        
        info_sizer.Add(
            wx.HyperlinkCtrl(
                parent=info_panel,
                id=wx.ID_ANY,
                label=_("Download the Python files"),
                url="%s/get/v%s.zip" % (common.IDE_WEBSITE,version)))
        
        info_sizer.Add(
            wx.HyperlinkCtrl(
                    parent=info_panel,
                    id=wx.ID_ANY,
                    label=_("Download the Windows binaries"),
                    url="%s/downloads/win_ess-ide_v%s.zip" % (common.IDE_WEBSITE,version)))
       
        info_panel.SetSizer(info_sizer)
        sub_sizer.Add(info_panel, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        # [Close]
        buttons_panel = wx.Panel(panel)
        buttons_sizer = wx.BoxSizer(wx.HORIZONTAL)
        close_button = wx.Button(parent=buttons_panel, id=wx.ID_CLOSE, label=_("Close"))
        buttons_sizer.Add(close_button)
        
        buttons_panel.SetSizer(buttons_sizer)
        sub_sizer.Add(buttons_panel, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)
        
                
        sizer.Add(sub_sizer, 0, wx.ALL, 5)
        panel.SetSizer(sizer)
        
        # Subscribe to the needed events
        close_button.Bind(wx.EVT_BUTTON, self.OnClose)
        
        close_button.SetFocus()

        # Size the dialog
        self.SetClientSize(panel.GetBestSize())       
            
    
    def OnClose(self, event):
        """User wishes to close the dialog
        Close the dialog"""      
        self.Close(True)    
    
    
