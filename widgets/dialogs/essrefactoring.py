# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Custom dialogs for refactoring"""

import wx

import os

class ExtractBlockDialog(wx.Dialog):
    """Extract block dialog"""
    
    def __init__(self, page, *args, **kwargs):
        wx.Dialog.__init__(self, *args, **kwargs)
        self._page = page
        
        panel = wx.Panel(self)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sub_sizer = wx.BoxSizer(wx.VERTICAL)
        
        # Block name:   [ ]
        # Insertion point: [ ]
        input_panel = wx.Panel(panel)
        input_sizer = wx.GridSizer(2, 2, 5, 5)
        input_sizer.Add(  # text ----- [ ]
            wx.StaticText(
                parent=input_panel,
                label=_("Block Name:")),
                flag=wx.ALIGN_CENTER_VERTICAL)
        self._blockname_text = wx.TextCtrl(
                                    parent=input_panel,
                                    size=(195,-1),
                                    style=wx.TE_PROCESS_ENTER)
        input_sizer.Add(self._blockname_text)
        #input_sizer.Add(wx.StaticText(parent=input_panel, label=_("Insertion Point:"), flag=wx.ALIGN_CENTER_VERTICAL) # text ----- [ ]
        #self._insert_point_ctrl = wx.ComboBox(parent=inputPanel, size=(195, -1))
        #self._insert_point_ctrl.SetEditable(False)
        #self._insert_point_ctrl.AppendItems((_("At the begin of the file"), _("Before the current block"), _("After the current block"), _("At the end of the file")))
        #self._insert_point_ctrl.Select(2)
        #input_sizer.Add(self._insert_point_ctrl)
        
        input_panel.SetSizer(input_sizer)
        sub_sizer.Add(input_panel, 0, wx.EXPAND | wx.ALL, 5)
        
        # Status message
        #status_panel = wx.Panel(panel)
        #status_sizer = wx.BoxSizer(wx.VERTICAL)
        #self._status_text = wx.StaticText(parent=status_panel, label="", size=(150, -1))
        #status_sizer.Add(self.__status_text)
        
        #status_panel.SetSizer(status_sizer)
        #sub_sizer.Add(status_panel, 0, wx.ALL, 5)
        
        # [Validate] [Close]
        buttons_panel = wx.Panel(panel)
        buttons_sizer = wx.GridSizer(1, 2, 5, 5)
        self._validate_button = wx.Button(parent=buttons_panel, label=_("Ok"), size=(125, -1))
        self._validate_button.Enable(False)
        buttons_sizer.Add(self._validate_button, 0, wx.ALIGN_CENTER_HORIZONTAL)
        close_button = wx.Button(parent=buttons_panel, label=_("Cancel"), size=(125, -1))
        buttons_sizer.Add(close_button, 0, wx.ALIGN_CENTER_HORIZONTAL)
        
        buttons_panel.SetSizer(buttons_sizer)
        sub_sizer.Add(buttons_panel, 0, wx.EXPAND | wx.ALL, 5)
        
        
        sizer.Add(sub_sizer, 0, wx.ALL, 5)
        panel.SetSizer(sizer)
        
        # Subscribe to the needed events
        self._blockname_text.Bind(wx.EVT_TEXT, self.OnBlockNameText)
        self.Bind(wx.EVT_TEXT_ENTER, self.OnValidate)
        self._validate_button.Bind(wx.EVT_BUTTON, self.OnValidate)
        close_button.Bind(wx.EVT_BUTTON, self.OnClose)
        
        # Set a default value for the block name
        filepath = self._page.GetFile().GetPath()
        if filepath is not None:
            basename = os.path.splitext(os.path.basename(filepath))[0]
            if basename.startswith("es_"):
                basename = basename[3:]
            
            self._blockname_text.SetValue("%s/" % basename)
        else:
            self._blockname_text.SetValue("namespace/blockname")
        
        self._blockname_text.SetFocus()

        # Size the dialog
        self.SetClientSize(panel.GetBestSize())       

        
    def OnBlockNameText(self, event): # TODO: control the input
        """User typed something in the block name field
        Enable/disable the validate button 
        """
        blockname = self._blockname_text.GetValue()
        if blockname:
            self._validate_button.Enable(True)
            
            #if blockName in self.__page.GetObjCode().GetBlockList():
            #    self._status_text.SetLabel("Conflicting block name!")
        else:
            self._validate_button.Enable(False)
            #self._status_text.SetLabel("")
        
    
    def OnValidate(self, event): # TODO: insertion points
        """User validates the input
        Extract the code 
        """
        #if self._insert_point_ctrl.GetSelection() == 0: # At the begin of the file    
        #elif self._insert_point_ctrl.GetSelection() == 1: # Before the current block
        #elif self._insert_point_ctrl.GetSelection() == 2: # After the current block
        #elif self._insert_point_ctrl.GetSelection() == 3: # At the end of the file
        self.Extract()
        
        # Close the dialog
        self.Close(True)
    
    
    def OnClose(self, event):
        """User wishes to close the dialog
        Close the dialog"""      
        self.Close(True)    
        
        
    def Extract(self):
        """Extract the block"""
        manager = self._page.GetCodeManager()
        blockname = self._blockname_text.GetValue()
        
        self._page.BeginUndoAction()
        
        # Get info about the text to extract and where to create the new block
        extracted_pos = self._page.GetSelectionStart()
        extracted_line = self._page.LineFromPosition(extracted_pos)
        extracted = self._page.GetSelectedText()
        extracted_indent = self._page.GetLineIndentation(extracted_line)
        
        # Replace the text with a block call
        self._page.ReplaceSelection("es_xdoblock %s" % blockname)
        
        # Create the new block
        insert_pos = self._page.GetTextLength()
        to_insert = """
block %s
{
%s
}
""" % (blockname[blockname.rfind("/") + 1:],extracted)
        self._page.InsertText(insert_pos, to_insert)
        
        # Fix the indentation and the syntax Highlight
        manager.FixIndent(
            self._page.LineFromPosition(insert_pos) + 2, self._page.GetLineCount()) 
            # + 2 => jump to the first { of the new block
        manager.Highlight(insert_pos, self._page.GetTextLength())
        self._page.SetLineIndentation(extracted_line, extracted_indent)
        manager.Highlight(extracted_pos, extracted_pos + len(extracted) + 1)    
        
        self._page.EndUndoAction()            
