# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Custom dialog for text searches"""

import wx
import wx.stc

class SearchDialog(wx.Dialog):
    """Text search dialog"""
    
    def __init__(self, editor, *args, **kwargs):
        wx.Dialog.__init__(self, *args, **kwargs)
        self._editor = editor
        
        panel = wx.Panel(self)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sub_sizer = wx.BoxSizer(wx.VERTICAL)
        
        # Find:         [ ]
        # Replace with: [ ]
        input_panel = wx.Panel(panel)
        input_sizer = wx.GridSizer(2, 2, 5, 5)
        input_sizer.Add(
            wx.StaticText( # text ----- [ ]
                parent=input_panel, label=_("Find:")),
            0,
            wx.ALIGN_CENTER_VERTICAL) 
        self._search_combo = wx.ComboBox(parent=input_panel, size=(125, -1))
        self._search_combo.SetWindowStyle(
            self._search_combo.GetWindowStyle() | wx.TE_PROCESS_ENTER)
        input_sizer.Add(self._search_combo)
        input_sizer.Add(
            wx.StaticText(  # text ----- [ ]
                parent=input_panel, label=_("Replace with:")),
            0,
            wx.ALIGN_CENTER_VERTICAL)
        self._replace_combo = wx.ComboBox(parent=input_panel, size=(125, -1))
        self._replace_combo.SetWindowStyle(
                self._replace_combo.GetWindowStyle() | wx.TE_PROCESS_ENTER)        
        input_sizer.Add(self._replace_combo)
        
        input_panel.SetSizer(input_sizer)
        sub_sizer.Add(input_panel, 0, wx.EXPAND | wx.ALL, 5)   
        
        # -- Direction --
        # [Forward] [Backward]
        # ---------------
        direction_panel = wx.Panel(panel)
        direction_sizer = wx.StaticBoxSizer(
                            wx.StaticBox(
                                parent=direction_panel, label=_("Direction")),
                            orient=wx.VERTICAL)
        grid_sizer = wx.GridSizer(1, 2, 5, 5)
        self._forward_radio = wx.RadioButton(
                                parent=direction_panel, label=_("Forward"), style=wx.RB_GROUP)
        grid_sizer.Add(self._forward_radio)
        self._backward_radio = wx.RadioButton(parent=direction_panel, label=_("Backward"))
        grid_sizer.Add(self._backward_radio)
        direction_sizer.Add(grid_sizer, 0, wx.ALL, 5)
        
        direction_panel.SetSizer(direction_sizer)
        sub_sizer.Add(direction_panel, 0, wx.EXPAND | wx.ALL, 5)
        
        # -- Scope --
        # [Current file] [All files]
        # [Selected lines]
        # -----------
        #scope_panel = wx.Panel(panel)
        #scope_sizer = wx.StaticBoxSizer(wx.StaticBox(parent=scope_panel, label= _("Scope")), orient=wx.VERTICAL)
        #grid_sizer = wx.GridSizer(1,2,5,5)
        ##vert_sizer = wx.BoxSizer(wx.VERTICAL)
        #self._scope_current_radio = wx.RadioButton(parent=scope_panel, label=_("Current file"), style=wx.RB_GROUP)
        #grid_sizer.Add(self._scope_current_radio)
        #self._scope_all_radio = wx.RadioButton(parent=scope_panel, label=_("All files"))
        #grid_sizer.Add(self._scope_all_radio)
        ##vert_sizer.Add(grid_sizer)
        ##self._scope_selected_radio = wx.RadioButton(parent=scope_panel, label=_("Selected lines"))
        ##vert_sizer.Add(self._scope_selected_radio, 0, wx.TOP | wx.BOTTOM, 5)
        #scope_sizer.Add(grid_sizer, 0, wx.ALL, 5) #scope_sizer.Add(vert_sizer)
        
        #scope_panel.SetSizer(scope_sizer)
        #sub_sizer.Add(scope_panel, 0, wx.EXPAND | wx.ALL, 5)
        
        # -- Options --
        # [Case sensitive] [Incremental]
        # [Word start]     [Whole word]
        # [Regular Expressions]
        # -------------
        options_panel = wx.Panel(panel)
        options_sizer = wx.StaticBoxSizer(
                            wx.StaticBox(
                                parent=options_panel, label=_("Options")), orient=wx.VERTICAL)
        grid_sizer = wx.GridSizer(2, 2, 5, 5)
        vert_sizer = wx.BoxSizer(wx.VERTICAL)
        self._case_box = wx.CheckBox(parent=options_panel, label=_("Case sensitive"))
        grid_sizer.Add(self._case_box)
        self._incremental_box = wx.CheckBox(parent=options_panel, label=_("Incremental"))
        grid_sizer.Add(self._incremental_box)
        self._word_start_box = wx.CheckBox(parent=options_panel, label=_("Word start"))
        grid_sizer.Add(self._word_start_box)
        self._whole_word_box = wx.CheckBox(parent=options_panel, label=_("Whole word"))
        grid_sizer.Add(self._whole_word_box)
        vert_sizer.Add(grid_sizer)
        self._regex_box = wx.CheckBox(parent=options_panel, label=_("Regular expressions"))
        vert_sizer.Add(self._regex_box, 0, wx.TOP | wx.BOTTOM, 5)
        options_sizer.Add(vert_sizer)
        
        options_panel.SetSizer(options_sizer)
        sub_sizer.Add(options_panel, 0, wx.EXPAND | wx.ALL, 5)
        
        # [Search]  [Replace/Find]
        # [Replace] [Replace All]
        #           [Close]
        buttons_panel = wx.Panel(panel)
        buttons_sizer = wx.GridSizer(3, 2, 5, 5)
        search_button = wx.Button(parent=buttons_panel, label=_("Find"), size=(125, -1))
        buttons_sizer.Add(search_button)
        replace_find_button = wx.Button(
                                parent=buttons_panel, label=_("Replace/Find"), size=(125, -1))
        buttons_sizer.Add(replace_find_button)
        replace_button = wx.Button(parent=buttons_panel, label=_("Replace"), size=(125, -1))
        buttons_sizer.Add(replace_button)
        replace_all_button = wx.Button(
                                parent=buttons_panel, label=_("Replace All"), size=(125, -1))
        buttons_sizer.Add(replace_all_button)
        buttons_sizer.Add((125, -1))
        close_button = wx.Button(parent=buttons_panel, label=_("Close"), size=(125, -1))
        buttons_sizer.Add(close_button)

        buttons_panel.SetSizer(buttons_sizer)
        sub_sizer.Add(buttons_panel, 0, wx.EXPAND | wx.ALL, 5)  
        
        sizer.Add(sub_sizer, 0, wx.ALL, 5)
        panel.SetSizer(sizer)
        
        
        # Subscribe to the needed events
        search_button.Bind(wx.EVT_BUTTON, self.OnFind)
        replace_find_button.Bind(wx.EVT_BUTTON, self.OnReplaceFind)
        replace_button.Bind(wx.EVT_BUTTON, self.OnReplace)
        replace_all_button.Bind(wx.EVT_BUTTON, self.OnReplaceAll)
        close_button.Bind(wx.EVT_BUTTON, self.OnClose)
        self.Bind(wx.EVT_TEXT_ENTER, self.OnEnter)
        self._search_combo.Bind(wx.EVT_TEXT, self.OnSearchText)
        self.Bind(wx.EVT_SHOW, self.OnShow)

        # Size the dialog
        self.SetClientSize(panel.GetBestSize())               
        
    
    def _SearchFlags(self):
        """Construct the search flags depending to the user choices"""
        flags = 0
        if self._case_box.IsChecked():
            flags |= wx.stc.STC_FIND_MATCHCASE
        if self._word_start_box.IsChecked():
            flags |= wx.stc.STC_FIND_WORDSTART
        if self._whole_word_box.IsChecked():
            flags |= wx.stc.STC_FIND_WHOLEWORD
        if self._regex_box.IsChecked():
            flags |= wx.stc.STC_FIND_REGEXP # http://www.yellowbrain.com/stc/regexp.html
        return flags
    
    
    def _SetSearchAnchor(self, page):
        """Set the search anchor
        page : The implied document"""
        #if not self._scope_selected_radio.GetValue():
        pos = page.GetCurrentPos()
        if self._backward_radio.GetValue():
            pos -= 1 # minus 1 to not continually find the same string
            
        page.SetSelection(pos, pos) 
        # => cancel the selection, otherwise SearchNext will keep find the same word
            
        page.SearchAnchor()
    
    
    def _Find(self, allow_recursion = True):
        """Find text in the whole document
        allow_recursion : If True, allow recursive search
        Returns the position where text was found, -1 otherwise"""
        page = self._editor.GetCurrentPage()             
        found = -1
        
        #if self._scope_current_radio.GetValue(): # or self._scope_selected_radio.GetValue()         
        text = self._search_combo.GetValue()
        flags = self._SearchFlags()   
        self._SetSearchAnchor(page)
        if self._forward_radio.GetValue():
            found = page.SearchNext(flags, text)  
        else:
            found = page.SearchPrev(flags, text)
        
        if found != -1:
            new_pos = found + len(text.encode("utf_8"))
            #self._editor.SetSelection(pagenum)
            page.GotoPos(new_pos)
            page.SetSelection(found, new_pos)
        elif allow_recursion:
            old_pos = page.GetCurrentPos()
            old_scroll = page.GetFirstVisibleLine()
            page.GotoPos(0)
            found = self._Find(False)
            # => no more recursive call - the whole document will be analyzed
            if found == -1:
                page.GotoPos(old_pos)
                page.ScrollToLine(old_scroll)
        
        return found

    def _Replace(self):
        """Replace the text found"""
        page = self._editor.GetCurrentPage()
        new_text = self._replace_combo.GetValue()
        page.ReplaceSelection(new_text)   
    
    def _ReplaceAll(self):
        """Search and replace all text"""
        #if self._scope_current_radio.GetValue():
        page = self._editor.GetCurrentPage()
        text = self._search_combo.GetValue()
        text_len = len(text.encode("utf_8"))
        new_text = self._replace_combo.GetValue()
        new_text_len = len(new_text.encode("utf_8"))
        flags = self._SearchFlags()

        page_len = page.GetTextLength()
        
        pos = 0
        nbFound = 0
        more = True
        while more:
            pos = page.FindText(pos, page_len, text, flags)
            new_pos = pos + text_len
            page.SetTargetStart(pos)
            page.SetTargetEnd(new_pos)

            if pos == -1:
                more = False             
            else:
                page.ReplaceTarget(new_text)
                page_len = page.GetTextLength() # text size has changed
                pos = new_pos
                nbFound += 1

        # FIXME: _ReplaceAll breaks the styles. wx.YieldIfNeeded() solves it but slows the search
        # So temporarily highlight the whole document
        if nbFound > 0:
            page.GetCodeManager().Highlight(0, page_len)
            
        wx.MessageBox(_("%(count)s replaced") % {"count" : nbFound}, _("Replace All"))
    
    
    def OnFind(self, event):
        """User used the Find button
        Do the search"""
        page = self._editor.GetCurrentPage()
        if page is not None:
            text = self._search_combo.GetValue()
            if text:
                if self._search_combo.FindString(text) == wx.NOT_FOUND:
                    self._search_combo.Append(text)
                self._Find()
                    
    def OnReplaceFind(self, event):
        """User used the Replace/Find button
        Search for the text and replace it
        """
        page = self._editor.GetCurrentPage()
        if page is not None:
            text = self._search_combo.GetValue()
            if text:
                if self._search_combo.FindString(text) == wx.NOT_FOUND:
                    self._search_combo.Append(text)
                new_text = self._replace_combo.GetValue()
                if new_text and self._replace_combo.FindString(new_text) == wx.NOT_FOUND:
                    self._replace_combo.Append(new_text)
                if self._Find() != -1:
                    self._Replace()
        
    def OnReplace(self, event):
        """User used the Replace button
        Replace the current selection
        """
        page = self._editor.GetCurrentPage()
        if page is not None:
            new_text = self._replace_combo.GetValue()
            if new_text and self._replace_combo.FindString(new_text) == wx.NOT_FOUND:
                self._replace_combo.Append(new_text)
            
            if page.GetSelectionStart() != page.GetSelectionEnd():
                self._Replace()
    
    def OnReplaceAll(self, event):
        """User used the ReplaceAll button
        Replace all matching text
        """     
        page = self._editor.GetCurrentPage() 
        if page is not None:
            text = self._search_combo.GetValue()
            if text:
                if self._search_combo.FindString(text) == wx.NOT_FOUND:
                    self._search_combo.Append(text)    
                new_text = self._replace_combo.GetValue()
                if new_text and self._replace_combo.FindString(new_text) == wx.NOT_FOUND:
                    self._replace_combo.Append(new_text)
                page.BeginUndoAction()
                self._ReplaceAll()
                page.EndUndoAction()
    
    def OnEnter(self, event):
        """User pressed Enter
        Start a search"""
        page = self._editor.GetCurrentPage()
        if page is not None:
            text = self._search_combo.GetValue()
            if text:
                self._search_combo.Append(text)
                self._Find()
                
    def OnSearchText(self, event):
        """User typed a char in the search combo box
        If incremental box is checked, start the search"""
        page = self._editor.GetCurrentPage()
        if page is not None:
            if self._incremental_box.IsChecked():
                self._Find()
            
    def OnShow(self, event):
        """Dialog is shown
        Update the search field with the current selection
        """
        page = self._editor.GetCurrentPage()
        if page is not None:
            selection = page.GetSelectedText()
            if selection:
                self._search_combo.SetValue(selection)
        self._search_combo.SetFocus()
            

    def OnClose(self, event):
        """User wishes to close the search dialog
        Close the dialog"""      
        self.Close(True)
