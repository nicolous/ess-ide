# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Custom dialogs for new projects/files"""

import os
import sys

import wx

import common
import widgets.editor as editor
import widgets.managers.manager as manager

class NewFileDialog(wx.Dialog):
    """New file dialog"""
    
    def __init__(self, editor_tabs, *args, **kwargs):
        """
        editorTabs : editor.EditorTabs instance where open the new file
        """
        wx.Dialog.__init__(self, *args, **kwargs)
        
        self._editor_tabs = editor_tabs
        code_managers = editor.Editor.GetManagerTypes()
        
        # button id => editor type
        self._buttons = {}
        
        panel = wx.Panel(self)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sub_sizer = wx.BoxSizer(wx.VERTICAL)
        
        # -- Select a file type -- 
        #   [ ]
        # ESS File
        # ------------------------
        choice_panel = wx.Panel(panel)
        choice_sizer = wx.StaticBoxSizer(
                        wx.StaticBox(
                            parent=choice_panel,
                            name=_("Select a file type")),
                            orient=wx.VERTICAL)
        
        buttons_panel = wx.Panel(choice_panel)
        buttons_sizer = wx.FlexGridSizer(2, 1 + len(code_managers), 20, 20)
        
        for Manager in [manager.BaseCodeManager] + code_managers:
            button_id = wx.NewId()
            logo = wx.Image(
                    os.path.join(
                        os.path.dirname(sys.argv[0]),
                        Manager.GetLogo()),wx.BITMAP_TYPE_PNG
                    ).ConvertToBitmap()
            button = wx.BitmapButton(buttons_panel, button_id, logo)
            button.Bind(wx.EVT_BUTTON, self.OnSelect)
            self._buttons[button_id] = Manager
            buttons_sizer.Add(button, 0, wx.ALIGN_CENTER_HORIZONTAL)
            
        buttons_sizer.Add(
            wx.StaticText(
                parent=buttons_panel, label=_("Text file")),
                0,
                wx.ALIGN_CENTER_HORIZONTAL)
        for Manager in code_managers:
            buttons_sizer.Add(
                wx.StaticText(
                    parent=buttons_panel,
                    label=_("%(language)s file") % {"language" : Manager.GetName()}),
                    0,
                    wx.ALIGN_CENTER_HORIZONTAL)        
        buttons_panel.SetSizer(buttons_sizer)
        
        choice_sizer.Add(buttons_panel, 0, wx.ALL, 10)
        
        choice_panel.SetSizer(choice_sizer)
        sub_sizer.Add(choice_panel, 0, wx.ALL, 5)
        
        
        #     Cancel
        cancel_panel = wx.Panel(panel)
        cancel_sizer = wx.BoxSizer(wx.VERTICAL)
        cancel_button = wx.Button(parent=cancel_panel, label=_("Cancel"), size=(125, -1))
        cancel_sizer.Add(cancel_button, 0, wx.ALIGN_CENTER)
        
        cancel_panel.SetSizer(cancel_sizer)
        sub_sizer.Add(cancel_panel, 0, wx.EXPAND)
        
        
        sizer.Add(sub_sizer, 0, wx.EXPAND | wx.ALL, 5)
        panel.SetSizer(sizer)
        
        # Subscribe to the needed events
        cancel_button.Bind(wx.EVT_BUTTON, self.OnCancel)

        # Size the dialog
        self.SetClientSize(panel.GetBestSize())        
        
        # Give the focus to the last defined button
        button.SetFocus()   

 
    def OnSelect(self, event):
        """The user selects a file type
        Close this dialog and open the new file
        """
        self.Close()
        self._editor_tabs.NewPage(_("New File"), self._buttons[event.GetId()])  

    def OnCancel(self, event):
        """The user cancels the operation
        Close this dialog
        """
        self.Close()
        
