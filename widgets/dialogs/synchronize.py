# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Custom dialogs to send the script on a server"""

import wx
import wx.stc

class FtpDialog(wx.Dialog):
    """Send the script on the server via FTP"""
    
    def __init__(self, *args, **kwargs):
        wx.Dialog.__init__(self, *args, **kwargs)
        
        # Notebook page corresponding to the file to send
        self._page = None
        
        panel = wx.Panel(self)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sub_sizer = wx.BoxSizer(wx.VERTICAL)
        
        # FTP: [ ]
        # Destination: [ ]        
        info_panel = wx.Panel(panel)
        #info_box = wx.StaticBoxSizer(
        #                wx.StaticBox(
        #                    parent=info_panel, label=_("FTP")),
        #                orient=wx.VERTICAL)
        info_sizer = wx.FlexGridSizer(4, 2, 5, 5)
        
        info_sizer.Add(
            wx.StaticText(
                parent=info_panel,
                label=_("Account:")),
                flag=wx.ALIGN_CENTER_VERTICAL)
        self._serverlist_combo = wx.ComboBox(
                                    parent=info_panel,
                                    size=(300, -1),
                                    style=wx.CB_READONLY)
        info_sizer.Add(self._serverlist_combo)
        self._serverlist_combo.SetFocus()
        
        info_sizer.Add(
            wx.StaticText(
                parent=info_panel,
                label=_("User:")),
                flag=wx.ALIGN_CENTER_VERTICAL)
        self._user_ctrl = wx.TextCtrl(
                                parent=info_panel,
                                size=(300, -1),
                                style=wx.TE_PROCESS_ENTER)
        info_sizer.Add(self._user_ctrl)
        
        info_sizer.Add(
            wx.StaticText(
                parent=info_panel,
                label=_("Password:")),
                flag=wx.ALIGN_CENTER_VERTICAL)
        self._password_ctrl = wx.TextCtrl(
                                parent=info_panel,
                                size=(300, -1),
                                style=wx.TE_PASSWORD | wx.TE_PROCESS_ENTER)
        info_sizer.Add(self._password_ctrl)
                
        info_sizer.Add(
            wx.StaticText(
                parent=info_panel,
                label=_("Destination:")),
                flag=wx.ALIGN_CENTER_VERTICAL)
        self._destination_combo = wx.ComboBox(parent=info_panel, size=(300, -1))
        self._destination_combo.SetWindowStyle(
            self._destination_combo.GetWindowStyle() | wx.TE_PROCESS_ENTER)
        info_sizer.Add(self._destination_combo)
        
        #info_box.Add(info_sizer)
        
        #info_panel.SetSizer(info_box)
        info_panel.SetSizer(info_sizer)
        sub_sizer.Add(info_panel)

        
        # [Send] [Close]
        buttons_panel = wx.Panel(panel)
        buttons_sizer = wx.GridSizer(1, 2, 5, 5)
        
        self._send_button = wx.Button(
                        parent=buttons_panel, label=_("Send"), size=(75, -1))
        self._send_button.Enable(False)
        buttons_sizer.Add(self._send_button , flag=wx.ALIGN_CENTER_HORIZONTAL)
        
        close_button = wx.Button(
                        parent=buttons_panel, label=_("Close"), size=(75, -1))
        buttons_sizer.Add(close_button, flag=wx.ALIGN_CENTER_HORIZONTAL)
        
        buttons_panel.SetSizer(buttons_sizer)
        sub_sizer.Add(buttons_panel, flag=wx.EXPAND | wx.ALL, border=5)
        
        
        sizer.Add(sub_sizer, 0, wx.ALL, 5)
        panel.SetSizer(sizer)
        
        # Size the dialog
        self.SetClientSize(panel.GetBestSize())
        
        # Subscribe to the needed events
        self.Bind(wx.EVT_COMBOBOX, self.OnServerChoice)
        self.Bind(wx.EVT_TEXT_ENTER, self.OnEnter)
        self._send_button .Bind(wx.EVT_BUTTON, self.OnSend)
        close_button.Bind(wx.EVT_BUTTON, self.OnClose)
                
    
    def _FillServerlist(self):
        """Fill the FTP server list"""
        self._serverlist_combo.Clear()
        profile = self._page.GetProfile()
        for server in profile.ftp.serverlist:
            self._serverlist_combo.Append(server.name)            
            
    def SetPage(self, page):
        """Set the notebook page corresponding to the file to send"""
        self._page = page
        if self._page is not None:
            self._FillServerlist()
            
    def Close(self, force):
        """Close the dialog"""
        self.SetPage(None)      
        wx.Dialog.Close(self, force)
        
    def _Send(self):
        """Send the file"""
        ftp_name = self._serverlist_combo.GetValue()
        target_dir = self._destination_combo.GetValue()
        ftp_user = self._user_ctrl.GetValue()
        ftp_password = self._password_ctrl.GetValue()
        
        code_manager = self._page.GetCodeManager()
        
        if code_manager.SendToFtp(ftp_name, ftp_user, ftp_password, target_dir):
            if target_dir not in self._destination_combo.GetItems():
                self._destination_combo.Append(target_dir)
            self.Close(True)
            
    
    def OnServerChoice(self, event):
        """The user selects a server in the combo box"""
        self._send_button.Enable(True)
        
        
    def OnEnter(self, event):
        """The user pressed Enter
        Send the file""" 
        self._Send()
        
   
    def OnSend(self, event):
        """The user choose to send
        Send the file"""
        self._Send()
        
        
    def OnClose(self, event):
        """The user wishes to close the dialog
        Close the dialog"""    
        self.Close(True)
        