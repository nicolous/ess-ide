# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Tool tabs
Currently only shows error descriptions"""

import wx

import os
import sys

class ErrorListCtrl(wx.ListCtrl):

    # Possible sort orders
    SORT_ASCENDING = 1
    SORT_DESCENDING = -1   
    
    # Column positions
    COL_FILEID = 4
    COL_TYPE = 0
    COL_FILE = 1 
    COL_LINE = 2
    COL_DESCRIPTION = 3
    
    def __init__(self, frame, *args, **kwargs):
        wx.ListCtrl.__init__(self, *args, **kwargs)
        
        self._frame = frame
        
        self._imagelist = wx.ImageList(16,16)
        
        basedir = os.path.dirname(sys.argv[0])
        self._img_warning = self._imagelist.Add(
                            wx.Image(
                                os.path.join(basedir, "resources/images/warning-16.png"),
                                wx.BITMAP_TYPE_PNG
                            ).ConvertToBitmap())
        self._img_error = self._imagelist.Add(
                            wx.Image(
                                os.path.join(basedir, "resources/images/error-16.png"),
                                wx.BITMAP_TYPE_PNG
                            ).ConvertToBitmap())
        self.SetImageList(self._imagelist, wx.IMAGE_LIST_SMALL)
        
        self.InsertColumn(ErrorListCtrl.COL_TYPE, " ")
        self.SetColumnWidth(ErrorListCtrl.COL_TYPE, 24)
        
        self.InsertColumn(ErrorListCtrl.COL_FILE, _("File"))
        self.SetColumnWidth(ErrorListCtrl.COL_FILE, 150)

        self.InsertColumn(ErrorListCtrl.COL_LINE, _("Line"))
        self.SetColumnWidth(ErrorListCtrl.COL_LINE, 50)
        
        self.InsertColumn(ErrorListCtrl.COL_DESCRIPTION, _("Description"))
        self.SetColumnWidth(ErrorListCtrl.COL_DESCRIPTION, 800)  

        # Must be the last insertion, otherwise the column width is reset
        self.InsertColumn(ErrorListCtrl.COL_FILEID, "File ID") # link the file and its errors    
        self.SetColumnWidth(ErrorListCtrl.COL_FILEID, 0)
        # we could use SetItemData, but we will use it for the sort instead

        # Subscribe to the needed events (see the callbacks for more info)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnErrorSelect)
        self.Bind(wx.EVT_LIST_INSERT_ITEM, self.OnErrorInsert)
        self.Bind(wx.EVT_LIST_COL_CLICK, self.OnErrorColClick)
        
        # Sort management 
        self._sort_order = [ErrorListCtrl.COL_FILE, ErrorListCtrl.SORT_ASCENDING] 
        #                  [Column id => order]
            
            
    def AddError(self, _code_manager, errtype, linenum, description):       
        """Append an error to the error list"""     
        filepath = _code_manager.GetFile().GetPath()
        if filepath is None:
            filepath = _("New File")
        self.Append(
            ("", os.path.basename(filepath), linenum, description, _code_manager.GetFileId()))
        
        img = self._img_error if errtype == "error" else self._img_warning
        self.SetItemImage(self.GetItemCount() - 1, img)
        
    
    def RemoveErrors(self, _code_manager):
        """Remove all errors reported by a particular code manager"""
        fileid = _code_manager.GetFileId()
        
        itemid = self.GetNextItem(-1)
        #while itemid != -1:
        #    if self.GetItem(itemid, ErrorListCtrl.COL_FILEID).GetText() == fileid:
        #        self.DeleteItem(itemid) # the next item becomes the current item
        #    else:
        #        itemid = self.GetNextItem(itemid)
        # => Works under Linux, but not Windows
        
        # Platform independent code:
        id_to_remove = []
        while itemid != -1:
            if self.GetItem(itemid, ErrorListCtrl.COL_FILEID).GetText() == fileid:
                id_to_remove.append(itemid)
            itemid = self.GetNextItem(itemid)
        while len(id_to_remove):
            self.DeleteItem(id_to_remove.pop())                
        
    
    def OnErrorInsert(self, event):
        """A new item is inserted into the error list
        TODO: Sort the items
        Currently: Does nothing"""    
        event.Skip()        
        
        
    def OnErrorColClick(self, event):
        """User clicked on a column of the error listctrl
        Sort the items depending to the column selected"""
        column = event.GetColumn()
        
        self._sort_order[0] = column

        if self._sort_order[1] == ErrorListCtrl.SORT_ASCENDING:
            # Previous order was asc, sort desc
            self._sort_order[1] = ErrorListCtrl.SORT_DESCENDING         
            
        else:
            # Previous order was desc, sort asc        
            self._sort_order[1] = ErrorListCtrl.SORT_ASCENDING         
                        
        self._Sort(self._sort_order[0],self._sort_order[1])                


    def OnErrorSelect(self, event):
        """User select an error
        Move the caret to the implied line"""
        itemid = self.GetFirstSelected()
        tabs = self._frame.GetEditorTabs()
        
        # Search for the related document
        file_id = self.GetItem(itemid, ErrorListCtrl.COL_FILEID).GetText()
        textctrl = None
        i = tabs.GetPageCount() - 1
        while i >= 0:
            page = tabs.GetPage(i)
            if file_id == page.GetFileId():
                textctrl = page
                break
            i -= 1
        
        # Go to the implied line
        line = int(self.GetItem(itemid, ErrorListCtrl.COL_LINE).GetText())
        tabs.ChangeSelection(textctrl.GetBookPosition())
        textctrl.GotoLine(line - 1)
        textctrl.ScrollToLine(line - 1)


    def _Sort(self, column, order):
        """Sort the content of the list by column id
        column: The column id
        order: ErrorListCtrl.SORT_*"""
        itemid = self.GetNextItem(-1)
        while itemid != -1:
            self.SetItemData(itemid, itemid)
            itemid = self.GetNextItem(itemid)

        self.SortItems(
            lambda data1,data2:
                order * cmp(
                    self.GetItem(self.GetItemData(data1), column).GetText(),
                    self.GetItem(self.GetItemData(data2), column).GetText())
                   )
        

class ToolsTabs(wx.Notebook):
    """Tool set"""
    
    def __init__(self, frame, *args, **kwargs):
        wx.Notebook.__init__(self, *args, **kwargs)
        self._frame = frame        

        # Prepare the error list
        self._errorlist = ErrorListCtrl(frame, self, style=wx.LC_REPORT | wx.LC_HRULES | wx.LC_VRULES)

        self.AddPage(self._errorlist, _("Error list"))
        
             
    def GetErrorList(self):
        return self._errorlist       
