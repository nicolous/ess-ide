# coding:Utf-8

#    Copyright 2010-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESS IDE.
#
#    ESS IDE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESS IDE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESS IDE.  If not, see <http://www.gnu.org/licenses/>.

"""Launch me"""

import sys
import os
import socket
import threading
import gettext
import locale

# The project "parsers" is required
sys.path.append(os.path.join(os.path.dirname(sys.argv[0]), "parsers"))

# The project "builders" is required
sys.path.append(os.path.join(os.path.dirname(sys.argv[0]), "builders"))

# I18n
try:
    # Try to find ess-ide.mo with the current locale
    locale_dir = os.path.join(os.path.dirname(sys.argv[0]), "resources/locale")
    locale_lang = locale.getdefaultlocale()[0][:2]
    gettext.translation("ess-ide", locale_dir, [locale_lang]).install(True)
except IOError:
    # File not found
    import __builtin__
    __builtin__.__dict__["_"] = lambda text: text # defines _
    
# Init all supported code managers
import widgets.managers.essmanager
import widgets.managers.kvmanager
import widgets.managers.pymanager  
widgets.editor.Editor.RegisterManagerType(widgets.managers.kvmanager.KvCodeManager)
widgets.editor.Editor.RegisterManagerType(widgets.managers.pymanager.PyCodeManager)
widgets.editor.Editor.RegisterManagerType(widgets.managers.essmanager.EssCodeManager)

import wx

import common
import autoupdate
import widgets.frame
import widgets.editor

# Socket used by the application
BIND_PORT = 35103

class AppServer(threading.Thread):
    """Listen a socket from which commands can be received by the application.
    As this object bind a socket, its constructor will throw a socket.error exception if
    an instance of the application is already running.
    
    Current implemented commands:
    * OPEN file name
    Open a file name. 
    Useful for duplicate application instances that fail to instantiate this object.
    Those duplicates can send file names to be open by the running instance of the ide.
    
    * EXIT
    Stop from listening the socket.
    """
    
    def __init__(self, app):
        """
        app : IdeApp instance 
        """
        threading.Thread.__init__(self)
        
        self._app = app

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.bind(("localhost", BIND_PORT))        
        
        # Does this thread can continue its job?
        self._continue = True
        
    
    def run(self):        
        try:
            while self._continue:
                try:
                    command = self._socket.recv(128)
                    
                    if command.startswith("OPEN "):
                        # Tell the application that a file has to be opened    
                            self._app.OpenFile(command[5:])
                            
                    elif command == "EXIT":
                        self._continue = False    
                        
                    else:
                        print "Unknown AppServer command '%s'" % command                       
                        
                except socket.error, e:
                    print "AppServer encountered an error while receiving: %s" \
                        % " ".join([str(arg) for arg in e.args])
        finally:
            self._socket.close()
    
    
    def exit(self):
        """Exit this thread
        Shortcut to send EXIT to the socket"""
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.connect(("localhost", BIND_PORT))
            sock.send("EXIT")
            sock.close()
        except BaseException, e:
            print "AppServer encountered an error while exiting: %s\n" % repr(e)
                
        
class IdeApp(wx.App):
    """This application"""  
    
    def __init__(self, with_server, *args, **kwargs):
        """
        withServer : If True, the application will launch an AppServer
        """
        wx.App.__init__(self, *args, **kwargs)
        
        self._with_server = with_server
        
        # Create the server
        self._server = None
        if self._with_server:
            self._server = AppServer(self)
            self._server.start()

        # Create the main frame
        self._frame = widgets.frame.MainFrame(parent=None, title="%s (v%s)" % (common.IDE_NAME, common.IDE_VERSION))
        self._frame.Show()

        # All arguments are script files to open
        for arg in sys.argv[1:]:
            tabs = self._frame.GetEditorTabs()            
            tabs.NewPage(os.path.basename(arg), None, arg)
            
        # Check for an application update
        autoupdate.UpdateNotifier(self, self._frame).start()
        # Should stop silently even if the app ends while receiving from the Internet
        
        
    def MainLoop(self):
        try:
            wx.App.MainLoop(self)
        finally:
            if self._with_server:
                self._server.exit()
        
        
    def OpenFile(self, filepath):
        """Tell the application to open a file"""
        evt = widgets.frame.FileToOpenEvent()
        evt.filepath = filepath
        wx.PostEvent(self._frame, evt)        
        

def main(): 
    try:
        IdeApp(True, redirect=False).MainLoop()
    except socket.error, e:
        # Another application instance exists
        # Tell it to open the files
        
        print "Another running instance exists"
        
        if len(sys.argv) > 1:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.connect(("localhost", BIND_PORT))
            for arg in sys.argv[1:]:
                sock.send("OPEN %s" % arg)
            sock.close()
        else:
            dialog = wx.MessageDialog(
                        None,
                        _("Another instance of %(app)s is running. Continue?") \
                            % {"app" : common.IDE_NAME},
                        _("Duplicate instance"),
                        wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)
            if dialog.ShowModal() == wx.ID_YES:
                IdeApp(False, redirect=False).MainLoop()
            dialog.Destroy()
        
        
if __name__ == "__main__":
    main()
